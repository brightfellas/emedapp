package ke.co.brightfellas.emed;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.List;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Visit;

/**
 * Created by iUwej on 9/7/14.
 */
public class VisitLoader extends AsyncTaskLoader<List<Visit>> {

    private List<Visit> visits;
    private DbAdapter db;

    public VisitLoader(Context context){
        super(context);
        db=new DbAdapter(getContext());
    }
    @Override
    public List<Visit> loadInBackground() {

        //initialize of null
        if(visits==null){
            visits=new ArrayList<Visit>();
        }

        db.open();
        Cursor cursor= db.fetchIncompleteVisits();
        while(cursor.moveToNext()){
            Cursor patientCursor=db.findPatient(cursor.getLong(2));
            visits.add(Visit.createFromCursors(cursor, patientCursor));
            patientCursor.close();
        }
        cursor.close();
        db.close();
        return visits;
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();

        //release the resources associated with the visits
        if(visits !=null){
            visits=null;
        }
    }


    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();
    }
}
