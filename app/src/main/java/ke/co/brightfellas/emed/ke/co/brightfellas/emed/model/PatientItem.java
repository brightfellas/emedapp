package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by iUwej on 8/23/14.
 */
public class PatientItem extends ModelItem {
    private Item item;
    private int quantity;
    private long _id;

    public long getVisit_id() {
        return visit_id;
    }

    private long visit_id;

    private long itemId;

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    @Override
    String getCreateSQL() {
        return "create table patient_items(_id integer primary key autoincrement," +
                "quantity integer not null," +
                "visit_id integer not null," +
                "sync_status integer," +
                "item_id int not null);";
    }

    public static final String TABLE_NAME="patient_items";
    public static final String[] COLS={"_id","quantity","visit_id","sync_status","item_id"};

    @Override
    ContentValues toContentValues() {
        ContentValues cv= new ContentValues();
        cv.put("quantity",quantity);
        cv.put("visit_id",visit_id);
        cv.put("item_id",item.getId());
        cv.put("sync_status",sync_status);

        return cv;
    }





    @Override
    String getDropSQL() {
        return "drop table if exists patient_items";
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public long get_id() {
        return _id;
    }

    public  PatientItem(){}

    public PatientItem(Item item, int quantity) {
        this.item = item;
        itemId=item.getId();
        this.quantity = quantity;
    }

    public PatientItem(Item item,int quantity, long visit_id){
        this(item,quantity);
        this.visit_id=visit_id;
        this.itemId=item.getId();
    }

    public static PatientItem createFromCursor(Cursor cursor){
        int quantity=cursor.getInt(1);
        long itemId=cursor.getLong(4);

        PatientItem item1= new PatientItem();
        item1.setItemId(itemId);
        item1.setQuantity(quantity);
        return item1;


    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return item+"\t"+quantity+"\tKsh "+item.getPrice()*quantity;
    }

    public String toJson(){
        return String.format("{\"item_id\":%d,\"units\":%d,\"price\":%.2f}",item.getId(),quantity,item.getPrice());
    }
}
