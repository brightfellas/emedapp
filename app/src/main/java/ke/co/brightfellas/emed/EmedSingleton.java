package ke.co.brightfellas.emed;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by iUwej on 9/12/14.
 */
public class EmedSingleton {

    private  static  EmedSingleton mInstance;
    private RequestQueue requestQueue;
    private ImageLoader mImageLoader;
    private  static Context mContext;

    private  EmedSingleton(Context context){
        mContext=context;
        requestQueue= getRequestQueue();

        mImageLoader= new ImageLoader(requestQueue, new ImageLoader.ImageCache(){

            private  final LruCache<String,Bitmap> cache= new LruCache<String,Bitmap>(20);

            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url,bitmap);
            }
        });

    }

    public static synchronized  EmedSingleton  getInstance(Context context){
        if(mInstance==null){
            mInstance= new EmedSingleton(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue(){

        if(requestQueue==null){
            requestQueue= Volley.newRequestQueue(mContext.getApplicationContext());

        }

        return requestQueue;

    }

    public <T> void addToRequestQueue(Request<T> request){
        getRequestQueue().add(request);
    }

    public ImageLoader getmImageLoader(){
        return  mImageLoader;
    }





}
