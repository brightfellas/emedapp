package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by iUwej on 8/27/14.
 */
public class PaymentView extends ModelItem {

    @Override
    String getDropSQL() {
        return "drop table if exists payment_views";
    }

    @Override
    String getCreateSQL() {
        return "create table payment_views(_id integer primary key autoincrement," +
                "method_id integer not null," +
                "amount double not null," +
                "sync_status integer," +
                "visit_id integer not null," +
                "pay_code text);";
    }

    public static final String TABLE_NAME="payment_views";
    public static final String[] COLS={"_id","method_id","amount","sync_status","visit_id","pay_code"};

    public ContentValues toContentValues(){
        ContentValues cv= new ContentValues();
        cv.put("method_id",method.getId());
        cv.put("amount",amount);
        cv.put("visit_id",visit_id);
        cv.put("sync_status",sync_status);
        cv.put("pay_code",pay_code);
        return cv;
    }

    private PaymentMethod method;
    private double amount;
    private long visit_id;
    private String pay_code;
    private long _id;

    public String getPay_code() {
        return pay_code;
    }

    public void setPay_code(String pay_code) {
        this.pay_code = pay_code;
    }

    public PaymentView(PaymentMethod m,double a){
        this.method=m;
        this.amount=a;
    }

    public static PaymentView createFromCursor(Cursor cursor, DbAdapter db){
        long methodId=cursor.getLong(cursor.getColumnIndexOrThrow("method_id"));
        long visit_id=cursor.getLong(cursor.getColumnIndexOrThrow("visit_id"));
        double amount=cursor.getLong(cursor.getColumnIndexOrThrow("amount"));
        long id=cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        Cursor methodCursor=db.findPaymentMethod(methodId);
        methodCursor.moveToFirst();
        PaymentMethod m=PaymentMethod.createFromCursor(methodCursor);
        PaymentView view= new PaymentView(m,amount,visit_id);
        view.set_id(id);
        return view;
    }


    public PaymentView(PaymentMethod m, double a, long visit_id){
        this(m,a);
        this.visit_id=visit_id;
    }

    public PaymentView(){}

    public PaymentMethod getMethod() {
        return method;
    }

    public void setMethod(PaymentMethod method) {
        this.method = method;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return method.getName()+"\n"+amount;
    }

    public String toJson(){
         return String.format("{\"paymenttype_id\":%d,\"amount\":%.2f,\"description\":\"Amount paid\"}",method.getId(),amount);
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }
}
