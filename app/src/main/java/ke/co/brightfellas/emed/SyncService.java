package ke.co.brightfellas.emed;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by iUwej on 9/8/14.
 */
public class SyncService extends Service {

    private  static SyncAdapter asyncAdapter=null;

    private static final Object syncAdapterLock= new Object();

    @Override
    public void onCreate() {
        super.onCreate();
        synchronized (syncAdapterLock){
            if(asyncAdapter==null)
                asyncAdapter=new SyncAdapter(getApplicationContext(),true);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return asyncAdapter.getSyncAdapterBinder();
    }
}
