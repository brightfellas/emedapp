package ke.co.brightfellas.emed;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by iUwej on 9/8/14.
 */
public class PostGsonRequest<T> extends JsonRequest<T>{

    private final  Class<T> type;

    public PostGsonRequest(String url,String requestBody,Class<T> type,Response.Listener<T> listener,Response.ErrorListener errorListener){

        super(Method.POST,url,requestBody,listener,errorListener);
        this.type=type;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
            try{

                String json= new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                JSONObject jobject= new JSONObject(json);
                json=jobject.getString("message");
                T object= ModelFactory.createFromJson(json,type);
                return Response.success(object,HttpHeaderParser.parseCacheHeaders(response));


            }

         catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
             }
            catch (JSONException e) {
                return Response.error(new VolleyError(e));
           }



    }
}
