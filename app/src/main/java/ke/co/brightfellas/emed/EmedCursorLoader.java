package ke.co.brightfellas.emed;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;

/**
 * Created by iUwej on 9/4/14.
 */
public class EmedCursorLoader extends AsyncTaskLoader<Cursor> {

    Queryable queryable=null;
    Cursor cursor;

    interface Queryable{
         public Cursor getCursor();
    }

    public EmedCursorLoader(Context context,Queryable queryable ){
        super(context);
        this.queryable=queryable;
    }

    @Override
    public Cursor loadInBackground() {
        cursor=queryable.getCursor();
        return cursor;
    }

    @Override
    public void deliverResult(Cursor data) {
        //super.deliverResult(data);
        if(isReset()){
            if(cursor !=null)
                onReleaseResource(cursor);
        }

        Cursor oldCursor=data;

        if(isStarted()){
            super.deliverResult(cursor);
        }

        if(oldCursor !=null){
            onReleaseResource(oldCursor);
        }
    }

    @Override
    protected void onStartLoading() {
       // super.onStartLoading();
        if(cursor !=null){
            deliverResult(cursor);
        }
        else forceLoad();
    }

    @Override
    protected void onStopLoading() {
       // super.onStopLoading();
        cancelLoad();
    }

    @Override
    public void onCanceled(Cursor data) {
        super.onCanceled(data);
        onReleaseResource(data);
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();

        //release all the resources associated with the cursor
        if(cursor !=null){
            onReleaseResource(cursor);
            cursor=null;
        }

    }

    protected void onReleaseResource(Cursor cursor){
        cursor.close();
    }
}
