package ke.co.brightfellas.emed;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Patient;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Visit;

/**
 * Created by iUwej on 8/25/14.
 */
public class PatientsFragment extends ListFragment  implements LoaderManager.LoaderCallbacks<List<Patient>>{

    private PatientsAdapter dataAdapter;
    private PatientCursorAdapter cursorAdapter;
    DbAdapter db;

    @Override
    public Loader<List<Patient>> onCreateLoader(int i, Bundle bundle) {
        return new ListDataLoader<Patient>(getActivity(), new ListDataLoader.ListFetcher<Patient>() {
            @Override
            public List<Patient> fetchList() {

                Cursor cursor=db.getPatients();
                ArrayList<Patient> patients= new ArrayList<>();
                while(cursor.moveToNext()){
                    patients.add(Patient.createFromCursor(cursor));
                }
                cursor.close();
                return patients;
            }
        });
    }

    @Override
    public void onLoadFinished(Loader<List<Patient>> listLoader, List<Patient> patientList) {

        Toast.makeText(getActivity(),"Patient number "+patientList.size(),Toast.LENGTH_SHORT).show();
        dataAdapter.setData(patientList);

        dataAdapter.notifyDataSetChanged();



    }

    @Override
    public void onLoaderReset(Loader<List<Patient>> listLoader) {

        dataAdapter.setData(null);
        dataAdapter.notifyDataSetChanged();

    }



    interface OnPatientNavigationListener{

        public void navigate(long patientId,long visitId);
    }



    OnPatientNavigationListener listener;

    private Response.Listener<JSONObject> postListener= new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            try {

                JSONObject jsonObject= response.getJSONObject("message");
                long patientId=jsonObject.getLong("patient_id");
                long visitId=jsonObject.getLong("id");
                Toast.makeText(getActivity(),"Visit Added",Toast.LENGTH_SHORT).show();
                listener.navigate(patientId,visitId);

            } catch (JSONException e) {
                Log.d("Parsing Error",e.getMessage());
                Toast.makeText(getActivity(),"Unable to add booking. Try again.", Toast.LENGTH_SHORT).show();


            }
        }
    };

    private Response.ErrorListener errorListener= new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

            Log.d("Error Status",error.networkResponse.statusCode+"");
           if(error !=null){
               if(error.networkResponse !=null){
                   if(error.networkResponse.statusCode==409){
                       try {
                           String response= new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers));
                            JSONObject j= new JSONObject(response);
                           JSONObject jsonObject= j.getJSONObject("message");
                           long patientId=jsonObject.getLong("patient_id");
                           long visitId=jsonObject.getLong("id");
                           Toast.makeText(getActivity(),"Using existing visit",Toast.LENGTH_SHORT).show();
                           listener.navigate(patientId,visitId);

                       } catch (UnsupportedEncodingException e) {
                           Log.d("ParseError",e.getMessage());
                       } catch (JSONException e) {
                           Log.d("Parse Error",e.getMessage());
                       }

                   }

               }
           }
        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dataAdapter= new PatientsAdapter(getActivity());
        db= new DbAdapter(getActivity());
        cursorAdapter= new PatientCursorAdapter(getActivity());
        setListAdapter(cursorAdapter);
        //setEmptyText("No patient");
        //setListShown(false);
        registerForContextMenu(getListView());
       // getLoaderManager().initLoader(0,null,this);
        new FetchData().execute();


    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //listener.navigate(id);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater=getActivity().getMenuInflater();
        inflater.inflate(R.menu.patients_context_menu,menu);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            listener=(OnPatientNavigationListener)activity;
        }catch(ClassCastException exe){
            throw new ClassCastException(activity.getClass().getName()+" must implement the navigation listener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       return inflater.inflate(R.layout.fragment_list,container,false);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info= (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

        switch(item.getItemId()){
            case R.id.action_add_visit:

                long id=db.addVisit(new Visit(info.id));

                //check for connectivity, post to server and then navigate

                Log.d("New Visit",id+"");
                Log.d("PatientId",info.id+"");
                listener.navigate(info.id,id);
                return true;
            default:
                return super.onContextItemSelected(item);

        }



    }

    public static class PatientsAdapter extends ArrayAdapter<Patient>{

        public PatientsAdapter(Context context){
            super(context,android.R.layout.simple_list_item_1,new ArrayList<Patient>());
        }

        public void setData(List<Patient> patients){
            if(patients !=null){
                for(Patient p: patients)
                    add(p);
            }

        }

        @Override
        public long getItemId(int position) {
            return getItem(position).get_id();
        }
    }



    class FetchData extends AsyncTask<Void,Void,Cursor>{
        @Override
        protected Cursor doInBackground(Void... voids) {
            db.open();
            return db.getPatients(false);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
           if(cursor !=null){
               cursorAdapter.changeCursor(cursor);
               cursorAdapter.notifyDataSetChanged();

           }
            else{
               Toast.makeText(getActivity(),"No patients available",Toast.LENGTH_SHORT).show();
           }

        }
    }


    class PatientCursorAdapter extends CursorAdapter{

        public PatientCursorAdapter (Context context){
            super(context,null,false);
        }

        @Override
        public long getItemId(int position) {
            Cursor cursor=getCursor();
            cursor.moveToPosition(position);
           return cursor.getLong(0);
        }



        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view= inflater.inflate(android.R.layout.simple_list_item_1,null);
            bindView(view,context,cursor);
            return view;


        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            //Log.d("BindView ",cursor.getLong(0)+" Id:"+cursor.getLong(1));
            ((TextView)view.findViewById(android.R.id.text1)).setText(cursor.getString(2)+" "+cursor.getString(3));

        }
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }
}
