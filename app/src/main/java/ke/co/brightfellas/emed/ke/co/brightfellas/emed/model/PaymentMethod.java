package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by iUwej on 8/21/14.
 */
public class PaymentMethod extends ModelItem{

    @Override
    String getDropSQL() {
        return "drop table if exists payment_methods";
    }

    @Override
    String getCreateSQL() {
        return "create table payment_methods(_id integer primary key," +
                "name text not null);";
    }

    @Override
    ContentValues toContentValues() {
        ContentValues cv= new ContentValues();
        cv.put("_id",_id);
        cv.put("name",name);
        return cv;
    }

    public static PaymentMethod createFromCursor(Cursor cursor){

        return  new PaymentMethod(cursor.getLong(0),cursor.getString(1));

    }

    public PaymentMethod(){};
    public PaymentMethod(long id,String name){
        this._id=id;
        this.name=name;
    }


    public static final String TABLE_NAME="payment_methods";
    public static final String[] COLS={"_id","name"};

    @SerializedName("id")
    private long _id;

    public void setId(long _id) {
        this._id = _id;
    }

    public long getId() {
        return _id;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
