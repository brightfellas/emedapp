package ke.co.brightfellas.emed;


import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by iUwej on 9/7/14.
 */
public class ListDataLoader<T> extends AsyncTaskLoader<List<T>> {

    private List<T> data;


    interface ListFetcher<T>{
        public List<T> fetchList();
    }

    ListFetcher<T> mFetcher;


    public ListDataLoader(Context ctx,ListFetcher<T> fetcher){
        super(ctx);

        this.mFetcher=fetcher;

    }



    @Override
    public List<T> loadInBackground() {
        if(data ==null) {
            data = new ArrayList<T>();
        }

         data=mFetcher.fetchList();
        return data;
    }

    @Override
    protected void onReset() {
        super.onReset();

        onStopLoading();
        if(data !=null)
            data=null;
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();
    }
}
