package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;



import java.util.List;

/**
 * Created by iUwej on 9/7/14.
 */
public class Billing {

    private long visitId;
    private double amountPaid;
    private double totalBill;

    public List<PaymentView> getPayViews() {
        return payViews;
    }

    public void setPayViews(List<PaymentView> payViews) {
        this.payViews = payViews;
    }

    private List<PaymentView> payViews;

    public long getVisitId() {
        return visitId;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

    public double getTotalBill() {
        return totalBill;
    }

    public Billing(long visitId,double totalBill,double amountPaid){
        this.visitId=visitId;
        this.totalBill=totalBill;
        this.amountPaid=amountPaid;
    }

    public double getAmountDue(){
        return  totalBill-amountPaid;
    }


}
