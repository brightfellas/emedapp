package ke.co.brightfellas.emed;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Diagnosis;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Item;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Patient;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.PaymentMethod;

/**
 * Created by iUwej on 9/4/14.
 */
public class InstantiateFragement extends Fragment {

    private ProgressBar progressBar;
    private TextView lblStatus;
    private EmedApplication app;
    private int[] status= {0,0,0,0};
    private Credential credential;
    private DbAdapter db;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        app=(EmedApplication)getActivity().getApplication();
        credential=app.getCredential();
        db= new DbAdapter(getActivity());
        db.open();
        if(app.isInitialized()){
            startActivity(new Intent(getActivity(),DashboardActivity.class));
        }
        else{
            progressBar.setVisibility(View.VISIBLE);
            fetchData();
        }


    }

    private void fetchData(){

        //fetch items here
        Type type= new TypeToken<List<Item>>(){}.getType();
        String url="http://lite.emed.co.ke/api/v1/items?api_token="+credential.getToken()+"&dbname="+credential.getDbname();
        GsonRequest<List<Item>> itemsRequest= new GsonRequest<>(url,type,null,itemsListener,errorListener);

        //fetch diagnosis here
        String url2="http://lite.emed.co.ke/api/v1/diagnoses?api_token="+credential.getToken()+"&dbname="+credential.getDbname();
        Type type2= new TypeToken<List<Diagnosis>>(){}.getType();
        GsonRequest<List<Diagnosis>> diagRequest= new GsonRequest<>(url2,type2,null,diagnosisListener,errorListener);

       // fetch patients here
        String url3= "http://lite.emed.co.ke/api/v1/patients?api_token="+credential.getToken()+"&dbname="+credential.getDbname();
        Type type3= new TypeToken<List<Patient>>(){}.getType();
        GsonRequest<List<Patient>> patientRequest= new GsonRequest<>(url3,type3,null,patientsListener,errorListener);

        //fetch payment types here
        String url4= "http://lite.emed.co.ke/api/v1/payment_types?api_token="+credential.getToken()+"&dbname="+credential.getDbname();
        Type type4=new TypeToken<List<PaymentMethod>>(){}.getType();
        GsonRequest<List<PaymentMethod>> payRequests= new GsonRequest<>(url4,type4,null,payListener,errorListener);

        lblStatus.setText("Status...");
        app.getRequestQueue().add(itemsRequest);

        app.getRequestQueue().add(patientRequest);
        app.getRequestQueue().add(payRequests);
        app.getRequestQueue().add(diagRequest);



    }

    @Override
    public void onResume() {
        super.onResume();
        //check internet connectivity

    }
    private Response.ErrorListener errorListener= new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

            addStatus("Error fetching data");
        }
    } ;

    private void addStatus(String message){
        lblStatus.append(message+"\n");
    }

    private Response.Listener<List<Item>> itemsListener= new Response.Listener<List<Item>>() {
        @Override
        public void onResponse(List<Item> response) {

            addStatus("Drugs and procedures added: "+response.size());
            for(Item item : response){
                long result =db.addItem(item);
                Log.d("Item Result Id",result+"");
            }
            addStatus("Items saved");

        }
    };

    private Response.Listener<List<Diagnosis>> diagnosisListener= new Response.Listener<List<Diagnosis>>() {
        @Override
        public void onResponse(List<Diagnosis> response) {
            addStatus("Diagnosis added: "+response.size());
            for(Diagnosis d: response){
                long result=db.addDiagnosis(d);
                Log.d("Diagnosis Result Id",result+"");

            }
            addStatus("diagnosis saved");
            progressBar.setVisibility(View.GONE);
            app.setAppIsInitialized(true);

            startActivity(new Intent(getActivity(),DashboardActivity.class));

        }
    };

    private Response.Listener<List<Patient>> patientsListener= new Response.Listener<List<Patient>>() {
        @Override
        public void onResponse(List<Patient> response) {
            addStatus("Patients added: "+response.size());
            for(Patient p: response){
                p.setSync_status(1);
                long result=db.addPatient(p);
                Log.d("Patients ResultId",result+"");
            }
            addStatus("Patients saved");

        }
    };



    private Response.Listener<List<PaymentMethod>> payListener= new Response.Listener<List<PaymentMethod>>() {
        @Override
        public void onResponse(List<PaymentMethod> response) {
            addStatus("Payment methods added: "+response.size());
            for(PaymentMethod p: response){
                long result=db.addPaymentMethod(p);
                Log.d("ResultId",result+"");
            }
            addStatus("Payment methods saved");

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_instantiate,container,false);
        progressBar=(ProgressBar)v.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.INVISIBLE);
        lblStatus=(TextView)v.findViewById(R.id.status);
        return v;
    }

    @Override
    public void onStop() {
        super.onStop();
        if(db != null)
            db.close();
    }
}
