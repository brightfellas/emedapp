package ke.co.brightfellas.emed;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Visit;

/**
 * Created by iUwej on 9/7/14.
 */
public class VisitFragment extends ListFragment  implements LoaderManager.LoaderCallbacks<List<Visit>>{


    private  VisitListAdapter mAdapter;
    private DbAdapter db;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText("No visit");
        registerForContextMenu(getListView());
        mAdapter= new VisitListAdapter(getActivity());
        db= new DbAdapter(getActivity());

        setListAdapter(mAdapter);



    }

    @Override
    public void onResume() {
        super.onResume();
        if(db==null){
            db = new DbAdapter(getActivity());
        }
        new FetchData().execute();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.fragment_list,container,false);
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public Loader<List<Visit>> onCreateLoader(int i, Bundle bundle) {
        return new VisitLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<List<Visit>> listLoader, List<Visit> visits) {
        mAdapter.setData(visits);
        mAdapter.notifyDataSetChanged();
        setListShown(true);


    }

    class FetchData extends AsyncTask<Void,Void,List<Visit>>{

        @Override
        protected List<Visit> doInBackground(Void... voids) {
            db.open();
            List<Visit> visits= new ArrayList<>();
            Cursor cursor= db.fetchIncompleteVisits();
            Log.d("Cursor Size",cursor.getCount()+" items");
            //testing this
            while(cursor.moveToNext()){
                long visitId=cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                Log.d("Visit_ID",visitId+"");

                long patientId=cursor.getLong(cursor.getColumnIndexOrThrow("patient_id"));
                Cursor patientCursor=db.findPatient(patientId);
                patientCursor.moveToFirst();
                visits.add(Visit.createFromCursors(cursor,patientCursor));
                patientCursor.close();
            }


            cursor.close();
            db.close();
            return visits;
        }

        @Override
        protected void onPostExecute(List<Visit> visits) {
            if(visits !=null){
                mAdapter.clear();
                mAdapter.setData(visits);
                mAdapter.notifyDataSetChanged();



            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater= getActivity().getMenuInflater();
        inflater.inflate(R.menu.visits_context_menu,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info= (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.action_edit_visit:
               //open the billing activity here
                Visit visit=mAdapter.getItem(info.position);
                Log.d("Visit selected",visit.get_id()+"");
                Log.d("Visit Info",visit.getPatient().toString());
                Intent intent= new Intent(getActivity(),BillingActivity.class);
                intent.putExtra("visit_id",visit.get_id());

                startActivity(intent);
                return  true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onLoaderReset(Loader<List<Visit>> listLoader) {

        mAdapter.setData(null);

    }

    public static class VisitListAdapter extends ArrayAdapter<Visit>{

        private final LayoutInflater inflater;

        public VisitListAdapter(Context context){
            super(context,android.R.layout.simple_list_item_2);
            inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).get_id();
        }

        public void setData(List<Visit> data){
            if(data !=null){
                for (Visit item: data)
                        add(item);
             notifyDataSetChanged();
            }
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view;
           view=(convertView==null)?inflater.inflate(R.layout.two_item_list_template,parent,false):convertView;
            Visit item=getItem(position);
            ((TextView)view.findViewById(R.id.txt_header)).setText(item.getPatient().toString());

            ((TextView)view.findViewById(R.id.txt_footer)).setText(item.getCreated_at());
            return view;
        }
    }
}
