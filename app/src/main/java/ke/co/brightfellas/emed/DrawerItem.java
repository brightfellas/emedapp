package ke.co.brightfellas.emed;

/**
 * Created by iUwej on 8/16/14.
 */
public class DrawerItem {

    public long id;
    public String title;

    public DrawerItem(long id,String title){
        this.id=id;
        this.title=title;

    }

    @Override
    public String toString() {
        return title;
    }
}
