package ke.co.brightfellas.emed;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by iUwej on 9/4/14.
 */
public class ActivationFragment extends Fragment {

    EditText txtCode;
    Button btnActivate;
    private EmedApplication app;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    interface  OnActivationCompleteListener{
        public void onActivated(String code);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        app=(EmedApplication)getActivity().getApplication();
        btnActivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(txtCode.getEditableText().toString().isEmpty())){
                    String code=txtCode.getEditableText().toString();
                    app.saveDbName(code);
                    listener.onActivated(code);


                }
                else{
                    Toast.makeText(getActivity(),"You must enter a code",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_activate,container,false);
        txtCode=(EditText)v.findViewById(R.id.txt_activation_code);
        btnActivate=(Button)v.findViewById(R.id.btn_activate);

        return v;
    }
    OnActivationCompleteListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener=(OnActivationCompleteListener)activity;
    }
}
