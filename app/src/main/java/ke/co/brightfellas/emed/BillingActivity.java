package ke.co.brightfellas.emed;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Visit;

/**
 * Created by iUwej on 9/4/14.
 */
public class BillingActivity extends ActionBarActivity implements BillingFragment.OnButtonItemClickedListener,FragmentSelectItems.OnProceduresSavedListener,PaymentFragment.OnPaymentCompleteListener ,DiagnosisFragment.OnDiagnosisAddedListener{

    private long visit_id;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        if(savedInstanceState ==null){

         showFragment(R.id.billing_fragment);
         visit_id=getIntent().getLongExtra("visit_id",-1);

        }
        else{

            visit_id=savedInstanceState.getLong("visit_id");

        }
        Log.d("Visit Id",visit_id+"");




    }

    private  void replaceFragment(Fragment f,boolean addTostack){


            getSupportFragmentManager().beginTransaction().replace(R.id.container,f).addToBackStack("billing").commit();




    }


    public void showFragment(int fragmentId){
        Fragment frag=null;
        Bundle bundle= new Bundle();
        bundle.putLong("visit_id",visit_id);
        switch (fragmentId){
            case R.id.billing_fragment:

                FragmentManager fm = getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                frag= new BillingFragment();
                frag.setArguments(bundle);
                replaceFragment(frag,true);
                break;
            case R.id.procedures_fragment:
                 frag= new FragmentSelectItems();
                frag.setArguments(bundle);
                replaceFragment(frag,false);
                break;
            case R.id.diagnosis_fragment:
                frag= new DiagnosisFragment();
                frag.setArguments(bundle);
                replaceFragment(frag,false);
                break;
            case R.id.payment_fragment:
                frag= new PaymentFragment();
                frag.setArguments(bundle);
                replaceFragment(frag,false);
                break;


        }

    }

    @Override
    public void buttonClick(int id) {
        if(id==R.id.close){
            //do some clean up here and close
            new Handler().post( new Runnable() {
                @Override
                public void run() {
                    Log.d("Closing Visits",visit_id+"");
                    DbAdapter db= new DbAdapter(BillingActivity.this);
                    db.open();

                    db.closeVisit(visit_id);
                    db.close();
                    finish();
                }
            });


        }
        else{
            showFragment(id);
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
            if(fm.getBackStackEntryCount()<1){
                finish();
            }
        else{
                super.onBackPressed();
            }



    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putLong("visit_id",visit_id);
    }

    @Override
    public void proceduresAdded(long patientId) {
        showFragment(R.id.billing_fragment);
    }

    @Override
    public void paymentCompleted(long visitId) {
        showFragment(R.id.billing_fragment);
    }

    @Override
    public void diagnosisAdded(long visitId) {
        showFragment(R.id.billing_fragment);
    }
}
