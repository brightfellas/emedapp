package ke.co.brightfellas.emed;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonElement;


import org.json.JSONException;
import org.json.JSONObject;

import ke.co.brightfellas.emed.R;

/**
 * Created by iUwej on 8/16/14.
 */
public class LoginFragment extends  Fragment {


        EditText txtUsername,txtPassword;

        private EmedApplication app;
        private ProgressDialog dialog;
        private  Button btnLogin;


        interface OnLoginCompleteListener{

            public void loginComplete();
        }

    OnLoginCompleteListener navListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            navListener= (OnLoginCompleteListener) activity;
        }
        catch (ClassCastException exe){
            throw new ClassCastException("Activity must implement the OnRegisterButtonClickedListener");
        }
    }

    @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            app= (EmedApplication) getActivity().getApplication();


           btnLogin.setOnClickListener(new  View.OnClickListener(){

               @Override
               public void onClick(View view) {
                  final String email=txtUsername.getEditableText().toString().trim();
                  final String password=txtPassword.getEditableText().toString().trim();

                   if(app.isInitialized()){
                       if(!email.isEmpty()  && !password.isEmpty()){
                           if(email.contentEquals(AppConfig.getUsername(getActivity()).trim()) && password.contentEquals(AppConfig.getPassword(getActivity()).trim())){
                               navListener.loginComplete();
                           }
                           else{
                               Toast.makeText(getActivity(),"Invalid credentials!",Toast.LENGTH_SHORT).show();
                           }
                       }

                       else{
                           Toast.makeText(getActivity(),"Credentials required!",Toast.LENGTH_SHORT).show();
                       }
                   }
                   else{

                       Response.ErrorListener errorListener = new Response.ErrorListener(){
                           @Override
                           public void onErrorResponse(VolleyError error) {
                               dialog.dismiss();

                               if (error.networkResponse != null) {
                                   if(error.networkResponse.statusCode==401){
                                       Toast.makeText(getActivity(),"Invalid credentials!",Toast.LENGTH_SHORT).show();
                                   }
                                   else{
                                       Toast.makeText(app,"Error signing in   "+error.getMessage(),Toast.LENGTH_SHORT).show();
                                   }
                               }



                           }
                       };

                       final Response.Listener<String> listener= new Response.Listener<String>() {
                           @Override
                           public void onResponse(String response) {

                               try {

                                   Log.d("Login Result",response);
                                   JSONObject jsonObject= new JSONObject(response);

                                   String access_token=jsonObject.getString("token");
                                   JSONObject juser= jsonObject.getJSONObject("user");
                                   String email=juser.getString("email");


                                   Log.d("AccessToken",access_token);
                                   app.setAccessToken(access_token);
                                   app.setUsername(email);
                                   app.setPassword(password);
                                   dialog.dismiss();
                                   navListener.loginComplete();

                               } catch (JSONException e) {
                                   Log.d("Error parsing data",e.getMessage());
                                   dialog.dismiss();
                               }

                           }
                       };

                       if(!email.isEmpty()  && !password.isEmpty()){
                           StringRequest stringRequest= new StringRequest(Request.Method.POST,"http://lite.emed.co.ke/login?username="+email+"&password="+password+"&dbname="+app.getDbName(),listener,errorListener);
                           dialog= ProgressDialog.show(getActivity(),"Sign In","Submitting...");

                           app.getRequestQueue().add(stringRequest);
                       }

                       else{
                           Toast.makeText(getActivity(),"Credentials required!",Toast.LENGTH_SHORT).show();
                       }

                   }




                  //startActivity(new Intent(getActivity(),DashboardActivity.class));

               }
           });



        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v=inflater.inflate(R.layout.fragment_login,container,false);
            txtUsername= (EditText) v.findViewById(R.id.txt_username);
            txtPassword=(EditText)v.findViewById(R.id.txt_password);
            btnLogin=(Button)v.findViewById(R.id.btn_signin);


            return v;
        }

}
