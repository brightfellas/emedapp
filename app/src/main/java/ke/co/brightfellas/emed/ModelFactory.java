package ke.co.brightfellas.emed;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

/**
 * Created by iUwej on 8/21/14.
 */
public class ModelFactory {

    public static Gson getDefaultGson(){
        DefaultExclusionStrategy strategy= new DefaultExclusionStrategy();
        return  new GsonBuilder().setPrettyPrinting().setExclusionStrategies(strategy).create();
    }

    public static <T> T createFromJson(String json,Type type){

        T object =getDefaultGson().fromJson(json,type);
        return object;
    }


    static class  DefaultExclusionStrategy implements ExclusionStrategy {

        /**
         * Force any class with the annotation {@link @SkipSerialization} to be ignored during
         * serialization and deserialization
         * @see SkipSerialization*/

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            // TODO Auto-generated method stub
            return clazz.getAnnotation(SkipSerialization.class)!=null;
        }

        /**
         * Force any filed with the annotation {@link @SkipSerialization} to be ignored during
         * serialization and deserialization
         * @see SkipSerialization*/

        @Override
        public boolean shouldSkipField(FieldAttributes field) {
            // TODO Auto-generated method stub
            return field.getAnnotation(SkipSerialization.class)!=null;
        }

    }




}
