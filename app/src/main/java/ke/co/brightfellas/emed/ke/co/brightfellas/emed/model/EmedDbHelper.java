package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by iUwej on 9/4/14.
 */
public class EmedDbHelper extends SQLiteOpenHelper {

    public static final String DBNAME="emed.db";
    public static final int VERSION=1;

    public EmedDbHelper(Context c){
        super(c,DBNAME,null,VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        new Diagnosis().onCreate(sqLiteDatabase);
        new Patient().onCreate(sqLiteDatabase);
        new Item().onCreate(sqLiteDatabase);
        new PaymentMethod().onCreate(sqLiteDatabase);
        new Visit().onCreate(sqLiteDatabase);
        new PatientItem().onCreate(sqLiteDatabase);
        new PaymentView().onCreate(sqLiteDatabase);
        new PatientDiagnosis().onCreate(sqLiteDatabase);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        new Diagnosis().onUpgrade(sqLiteDatabase,newVersion,oldVersion);
        new Patient().onUpgrade(sqLiteDatabase,newVersion,oldVersion);
        new Item().onUpgrade(sqLiteDatabase,newVersion,oldVersion);
        new PaymentMethod().onUpgrade(sqLiteDatabase,newVersion,oldVersion);
        new Visit().onUpgrade(sqLiteDatabase,newVersion,oldVersion);
        new PatientItem().onUpgrade(sqLiteDatabase,newVersion,oldVersion);
        new PaymentView().onUpgrade(sqLiteDatabase,newVersion,oldVersion);
        new PatientDiagnosis().onUpgrade(sqLiteDatabase,newVersion,oldVersion);
    }
}
