package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;

/**
 * Created by iUwej on 9/14/14.
 */
public class Report {

    private  String date;
    private int totalVisits;
    private double income;

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public int getTotalVisits() {
        return totalVisits;
    }

    public void setTotalVisits(int   totalVisits) {
        this.totalVisits = totalVisits;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String toHtml(){
        StringBuilder builder = new StringBuilder();
        builder.append("<h4> Report for: "+date+"<h4>");
        builder.append("<dl>");
        builder.append("<dt>Total Visits</dt>");
        builder.append("<dd>"+totalVisits+"</dd>");
        builder.append("<dt>Income</dt>");
        builder.append("<dd>"+income+"</dd>");
        builder.append("</dl>");

        return builder.toString();



    }

}
