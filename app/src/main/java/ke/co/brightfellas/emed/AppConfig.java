package ke.co.brightfellas.emed;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by iUwej on 9/12/14.
 */
public class AppConfig {

    public static Credential getCredentials(Context context){

        return new Credential(getAccessToken(context),getDbName(context));

    }

    public static String getAccessToken(Context ctx){
        final SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(ctx);
        return prefs.getString("access_token","");
    }

    public static String getDbName(Context context){

            final SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(context);
            return prefs.getString("db_name",null);

    }

    public static String getUsername(Context ctx){
        final SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(ctx);
        return prefs.getString("username",null);
    }

    public  static String getPassword(Context context){

        final SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("password",null);

    }



}
