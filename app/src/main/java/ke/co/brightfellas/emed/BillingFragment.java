package ke.co.brightfellas.emed;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Billing;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;

/**
 * Created by iUwej on 9/7/14.
 */
public class BillingFragment extends Fragment  implements View.OnClickListener{

    private Button btnItems,btnDiagnosis,btnPayment,btnClose;
    private Billing billing;
    private ProgressDialog progressDialog;
    private DbAdapter db;
    private long visitId;

    interface OnButtonItemClickedListener{

        public void buttonClick(int id);

    }

    OnButtonItemClickedListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            listener= (OnButtonItemClickedListener) activity;
        }
        catch (ClassCastException exe){
            throw new ClassCastException(activity.getClass().getName()+" must implement the OnButtonItemClickedListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState ==null){
            visitId=getArguments().getLong("visit_id");
        }
        else{
            visitId=savedInstanceState.getLong("visit_id");
        }
        btnClose.setOnClickListener(this);
        btnPayment.setOnClickListener(this);
        btnDiagnosis.setOnClickListener(this);
        btnItems.setOnClickListener(this);
        db= new DbAdapter(getActivity());
        new BillingLoader().execute(visitId);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("visit_id",visitId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view= inflater.inflate(R.layout.fragment_billing,container,false);
        btnItems=(Button)view.findViewById(R.id.btn_procedures);
        btnDiagnosis=(Button)view.findViewById(R.id.btn_diagnosis);
        btnPayment=(Button)view.findViewById(R.id.btn_payment);
        btnClose=(Button)view.findViewById(R.id.btn_close);
        btnClose.setVisibility(View.INVISIBLE);
        return view;
    }

    @Override
    public void onClick(View view) {

        if(view.equals(btnItems)){
            listener.buttonClick(R.id.procedures_fragment);
        }
        else if(view.equals(btnPayment)){
            listener.buttonClick(R.id.payment_fragment);
        }
        else if(view.equals(btnDiagnosis)){
            listener.buttonClick(R.id.diagnosis_fragment);
        }
        else{

            listener.buttonClick(R.id.close);

        }


    }

    class BillingLoader extends AsyncTask<Long,Void,Billing> {

        @Override
        protected Billing doInBackground(Long... ids) {
            long visitId=ids[0];
            db.open();
            double totalAmount=0;
            double totalPaid=0;

            //fetch all the patient items
            Cursor cursor=db.fetchVisitItems(visitId);
            Log.d("Visit Items: ", visitId + ": " + cursor.getCount());
            while(cursor.moveToNext()){
                int quantity=cursor.getInt(1);
                long itemId=cursor.getLong(4);
                Cursor itemCursor=db.findItem(itemId);
                itemCursor.moveToFirst();
                double price=itemCursor.getDouble(4);
                totalAmount += (quantity * price);
            }

            //fetch all amount paid here and calculate the total
            Cursor paymentCursor=db.getVisitPayments(visitId);
            Log.d("Paid found: ",paymentCursor.getCount()+"");
            while(paymentCursor.moveToNext()){
                double paid=paymentCursor.getDouble(2);
                totalPaid+=paid;
            }

            return new Billing(visitId,totalAmount,totalPaid);

        }

        @Override
        protected void onPostExecute(Billing billing) {
            progressDialog.dismiss();
            if(billing !=null){
                if(billing.getAmountDue()<=0){
                    btnClose.setVisibility(View.VISIBLE);
                }
            }
            else{
                Log.d("Billing","the billing is null find out why");
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog= ProgressDialog.show(getActivity(), "Emed", "loading bill...");
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }
}
