package ke.co.brightfellas.emed;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.AvoidXfermode;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Patient;

/**
 * Created by iUwej on 8/19/14.
 */
public class RegisterPatientFragment extends Fragment {

    private EditText txtFirstName,txtLastName,txtMobile,txtNextOfKin,txtDate,txtIdNo;
    private Spinner spGender;
    private Button btnSave;
    private  ProgressDialog progressDialog;
    private EmedApplication app;
    DbAdapter db;

    private Calendar myCalendar;


    interface OnItemAddedListener{

        public void itemAdded(long Id);
    }

    OnItemAddedListener navListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            navListener =(OnItemAddedListener)activity;
        }
        catch (ClassCastException exe){
            throw new ClassCastException(activity.getClass().getName()+" should implement the OnItemAddedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(db !=null)
            db.open();
    }








    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        app=(EmedApplication)getActivity().getApplication();
        db=new DbAdapter(getActivity());
        db.open();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(entriesValid()){
                    Patient p= createPatient();
                    p.setSync_status(0);
                    long result= db.addPatient(p);
                    if(result >0)
                        Toast.makeText(getActivity(),"Patient added",Toast.LENGTH_SHORT).show();
                    //check application mode if connected post to server
                    navListener.itemAdded(result);

                }
                else{
                    Toast.makeText(getActivity(),"All fields are required",Toast.LENGTH_SHORT).show();
                }
             }

        });
        myCalendar=Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {

                myCalendar.set(Calendar.YEAR,year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateTextField();


            }
        };
        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerDialog dialog= new DatePickerDialog(getActivity(),date,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();

            }
        });
    }

    private void updateTextField() {

        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
        txtDate.setText(dateFormat.format(myCalendar.getTime()));

    }

    public boolean entriesValid(){



        EditText[] txtValues={txtFirstName,txtLastName,txtMobile,txtIdNo,txtDate,txtNextOfKin};
        for(EditText txt: txtValues){
            if(txt.getEditableText().toString().isEmpty()){
                return false;
            }
        }
        return true;

    }




    //include validations here
    private Patient createPatient(){
        return new Patient(txtFirstName.getEditableText().toString(),txtLastName.getEditableText().toString(),spGender.getSelectedItem().toString(),txtDate.getEditableText().toString(),txtNextOfKin.getEditableText().toString(),txtMobile.getEditableText().toString(),txtIdNo.getEditableText().toString());

    }

    @Override
    public void onPause() {
        super.onPause();
        if(db !=null)
            db.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.fragment_register_patient,container,false);
        txtFirstName=(EditText)v.findViewById(R.id.txt_first_name);
        txtLastName=(EditText)v.findViewById(R.id.txt_last_name);
        txtMobile=(EditText)v.findViewById(R.id.txt_mobile);
        txtNextOfKin=(EditText)v.findViewById(R.id.txt_next_of_kin);
        txtDate=(EditText)v.findViewById(R.id.txt_date);
        txtIdNo=(EditText)v.findViewById(R.id.txt_id_no);
        btnSave=(Button)v.findViewById(R.id.btn_save);
        spGender=(Spinner)v.findViewById(R.id.sp_gender);
        return v;
    }
}
