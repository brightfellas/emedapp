package ke.co.brightfellas.emed;

/**
 * Created by iUwej on 8/21/14.
 */
public class Credential {
    private String token;
    private String dbname;

    public Credential(String token,String dbname){
        this.token=token;
        this.dbname=dbname;
    }

    public String getDbname() {
        return dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
