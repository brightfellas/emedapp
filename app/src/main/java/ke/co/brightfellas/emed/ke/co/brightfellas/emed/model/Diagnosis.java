package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

/**
 * Created by iUwej on 8/19/14.
 */
public class Diagnosis   extends  ModelItem{


    @SerializedName("id")
    private long _id;
    private String name;
    private String ICPC2;


    public ContentValues toContentValues(){

        ContentValues cv= new ContentValues();
        cv.put("_id",_id);
        cv.put("name",name);
        cv.put("ICPC2",ICPC2);
        return cv;
    }

    public  static Diagnosis createDiagnosisFromCursor(Cursor c){
        return new Diagnosis(c.getLong(0),c.getString(1),c.getString(2));

    }

    public Diagnosis(){};
    public Diagnosis(long id,String name,String icpc2){
        this._id=id;
        this.name=name;
        this.ICPC2=icpc2;
    }


    public long getId(){
        return _id;
    }

    public  void setId(long id){
        _id=id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getICPC2() {
        return ICPC2;
    }

    public void setICPC2(String ICPC2) {
        this.ICPC2 = ICPC2;
    }

    @Override
    public String toString() {
        return String.format("%s\n%s\n",name,ICPC2);
    }

    @Override
    String getCreateSQL() {
        return "create table diagnosis(_id integer primary key ," +
                "name text not null," +
                "ICPC2 text not null);";
    }

    public static final String[] COLS={"_id","name","ICPC2"};

    @Override
    String getDropSQL() {
        return "drop table if exists diagnosis";
    }

    public static final String TABLE_NAME="diagnosis";
}
