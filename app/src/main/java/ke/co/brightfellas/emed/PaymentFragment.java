package ke.co.brightfellas.emed;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Billing;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.PaymentMethod;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.PaymentView;

/**
 * Created by iUwej on 8/23/14.
 */
public class PaymentFragment extends ListFragment{

    private EditText txtAmountDue;
    private Spinner txtSearch;
    private TextView lblAmountDue,lblTotal;
    private ArrayAdapter<PaymentMethod> paymentMethodArrayAdapter;
    private EmedApplication app;
    private ArrayAdapter<PaymentView> payViewAdapter;
    private ArrayList<PaymentView> addViews;

    private PaymentMethod selected=null;
    private Button btnSave,btnAdd;
    private long visitId;
    private ProgressDialog progressDialog=null;
    private double amountPaid;
    private  Billing billing;
    private DbAdapter db;



    public void onLoadFinished(Billing billing) {

        this.billing=billing;
        lblAmountDue.setText("Total Amount: "+billing.getTotalBill());
        amountPaid=billing.getAmountPaid();
        lblTotal.setText("Total Paid: "+amountPaid);


    }



    interface  OnPaymentCompleteListener{

        public void paymentCompleted(long visitId);
    }
    OnPaymentCompleteListener navListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            navListener=(OnPaymentCompleteListener)activity;
        }
        catch (ClassCastException exe){
            throw new ClassCastException(activity.getClass().getName()+" does not implement OnPaymentCompleteListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState==null){
            visitId=getArguments().getLong("visit_id");

        }
        else{
            visitId=savedInstanceState.getLong("visit_id");

        }
        db= new DbAdapter(getActivity());
        addViews= new ArrayList<>();
        app=(EmedApplication)getActivity().getApplication();
        paymentMethodArrayAdapter= new ArrayAdapter<PaymentMethod>(getActivity(),android.R.layout.simple_list_item_1,new ArrayList<PaymentMethod>());
        txtSearch.setAdapter(paymentMethodArrayAdapter);



        payViewAdapter= new ArrayAdapter<PaymentView>(getActivity(),android.R.layout.simple_list_item_1,new ArrayList<PaymentView>()){

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view=convertView;
                if(view==null){
                    LayoutInflater inflater=(LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    view=inflater.inflate(R.layout.items_template,null);

                }
                PaymentView payView=getItem(position);
                ((TextView)view.findViewById(R.id.lbl_item)).setText(payView.getMethod().getName());
                ((TextView)view.findViewById(R.id.lbl_amount)).setText("Ksh "+payView.getAmount());
                return view;
            }
        };
        setListAdapter(payViewAdapter);

        txtAmountDue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEND || i == EditorInfo.IME_ACTION_DONE || i == EditorInfo.IME_ACTION_GO){

                        addPayment();
                        return true;

                }
                return false;
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        saveToDb();
                        navListener.paymentCompleted(visitId);
                    }
                });



                //check for connection then save to server

               // saveToServer();


            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPayment();
            }
        });
        getListView().setDivider(getResources().getDrawable(R.drawable.black_background));
        getListView().setDividerHeight(1);
        registerForContextMenu(getListView());

        new LoadPayment().execute();
        new BillingLoader().execute(visitId);




    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info=(AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        PaymentView payView=payViewAdapter.getItem(info.position);
        // remove from the adapter
        switch (item.getItemId()){
            case R.id.remove:
                db.open();
                long id=payView.get_id();
                if(db.deletePayment(id)){
                    Log.d("Record Deleted","Record with Id "+id);
                }
                double amount=payView.getAmount();
                payViewAdapter.remove(payView);
                addViews.remove(payView);
                payViewAdapter.notifyDataSetChanged();
                amountPaid-=amount;
                lblTotal.setText("Total Paid: "+amountPaid);

        }


        return super.onContextItemSelected(item);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater=getActivity().getMenuInflater();
        inflater.inflate(R.menu.edit_items,menu);

    }

    class BillingLoader extends AsyncTask<Long,Void,Billing>{

        @Override
        protected Billing doInBackground(Long... ids) {
            long visitId=ids[0];
            db.open();
            double totalAmount=0;
            double totalPaid=0;

            //fetch all the patient items
            Cursor cursor=db.fetchVisitItems(visitId);
            Log.d("Visit Items: ",visitId+": "+cursor.getCount());
            while(cursor.moveToNext()){
                int quantity=cursor.getInt(1);
                long itemId=cursor.getLong(4);
                Cursor itemCursor=db.findItem(itemId);
                itemCursor.moveToFirst();
                double price=itemCursor.getDouble(4);
                totalAmount += (quantity * price);
            }

            //fetch all amount paid here and calculate the total
            Cursor paymentCursor=db.getVisitPayments(visitId);
            Log.d("Paid found: ",paymentCursor.getCount()+"");
            ArrayList<PaymentView> views= new ArrayList<>();
            while(paymentCursor.moveToNext()){
                double paid=paymentCursor.getDouble(2);
                totalPaid+=paid;
                views.add(PaymentView.createFromCursor(paymentCursor,db));
            }
            Billing bill= new Billing(visitId,totalAmount,totalPaid);
            bill.setPayViews(views);

            return bill;

        }

        @Override
        protected void onPostExecute(Billing billing) {
            progressDialog.dismiss();
            if(billing !=null){
                onLoadFinished(billing);
                for(PaymentView view: billing.getPayViews()){
                    payViewAdapter.add(view);
                }
                payViewAdapter.notifyDataSetChanged();
            }
            else{
                Log.d("Billing", "the billing is null find out why");
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog= ProgressDialog.show(getActivity(),"Emed","loading bill...");
        }
    }


    class LoadPayment extends AsyncTask<Void,Void,List<PaymentMethod>>{

        @Override
        protected List<PaymentMethod> doInBackground(Void... voids) {
            db.open();
            Cursor cursor=db.getPaymentMethods();
            ArrayList<PaymentMethod> methods= new ArrayList<>();
            while(cursor.moveToNext()){
                methods.add(PaymentMethod.createFromCursor(cursor));
            }
            cursor.close();
            return methods;
        }

        @Override
        protected void onPostExecute(List<PaymentMethod> paymentMethods) {
            Log.d("Payments fetched",paymentMethods.size()+"");
            for(PaymentMethod m: paymentMethods){
                paymentMethodArrayAdapter.add(m);
            }

            paymentMethodArrayAdapter.notifyDataSetChanged();
        }
    }

    private void addPayment() {
        if(!(txtAmountDue.getEditableText().toString().isEmpty())) {
            double amount = Double.valueOf(txtAmountDue.getEditableText().toString());

            if((amountPaid +amount)>billing.getTotalBill()){
                Toast.makeText(getActivity(),"Total exceeds bill",Toast.LENGTH_SHORT).show();
                return;
            }

            amountPaid += amount;
            PaymentView view=new PaymentView((PaymentMethod) txtSearch.getSelectedItem(), amount,visitId);
            payViewAdapter.add(view);
            addViews.add(view);
            payViewAdapter.notifyDataSetChanged();
            lblTotal.setText("Total: Ksh" + amountPaid);
        }
        else{
            Toast.makeText(getActivity(),"Amount can not be empty",Toast.LENGTH_SHORT).show();
        }


    }

    public void saveToDb(){

        progressDialog= ProgressDialog.show(getActivity(),"Emed","Saving to db");
        for(PaymentView payment: addViews){



            long result=db.addPaymentView(payment);
            Log.d("Visit Payment","Item added with id: "+result);

        }
        progressDialog.dismiss();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_payment,container,false);
        txtAmountDue=(EditText)view.findViewById(R.id.txt_amount);
        txtSearch=(Spinner)view.findViewById(R.id.txt_search_payments);
        lblAmountDue=(TextView)view.findViewById(R.id.lbl_amount_due);
        lblTotal=(TextView)view.findViewById(R.id.lbl_total);
        btnSave=(Button)view.findViewById(R.id.btn_save);
        btnAdd=(Button)view.findViewById(R.id.btn_add);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("visit_id",visitId);

    }


    @Override
    public void onPause() {
        super.onPause();
        if(db !=null)
            db.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(db != null)
            db.open();
    }
}
