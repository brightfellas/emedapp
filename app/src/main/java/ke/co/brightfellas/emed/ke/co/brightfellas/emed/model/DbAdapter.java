package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import ke.co.brightfellas.emed.PaymentFragment;

/**
 * Created by iUwej on 9/4/14.
 */
public class DbAdapter {

    private Context mContext;
    private SQLiteDatabase db;
    private  EmedDbHelper helper;

    public  DbAdapter(Context c){
        mContext=c;
    }

    public DbAdapter open() throws SQLiteException{
        helper= new EmedDbHelper(mContext);
        db=helper.getWritableDatabase();
        return this;
    }

    public void close(){
        helper.close();
    }

    public long addPatient(Patient p){
        ContentValues cv= p.toContentValues();
        return db.insert(Patient.TABLE_NAME,null,cv);

    }

    public long addItem(Item item){
        return db.insert(Item.TABLE_NAME,null,item.toContentValues());
    }

    public long addDiagnosis(Diagnosis d){
        return db.insert(Diagnosis.TABLE_NAME,null,d.toContentValues());
    }

    public long addVisit(Visit visit){
        long patientId=visit.getPatient_id();
        long result=db.insert(Visit.TABLE_NAME,null,visit.toContentValues());
        if(result>0)
            setPatientStatus(patientId,true);
        return result;
    }

    public long addPaymentMethod(PaymentMethod method){
        return db.insert(PaymentMethod.TABLE_NAME,null,method.toContentValues());
    }

    public long addPaymentView(PaymentView view){
        return db.insert(PaymentView.TABLE_NAME,null,view.toContentValues());
    }




    public Cursor getPatients(){
        return db.query(Patient.TABLE_NAME,Patient.COLS,null,null,null,null,null);
    }

    public Cursor getUnsycedPatients(){
        return db.query(Patient.TABLE_NAME,Patient.COLS,"sync_status=0",null,null,null,null);
    }


    public Cursor getPatients(boolean hasVisits){
        int status= hasVisits?1:0;
        return db.query(Patient.TABLE_NAME,Patient.COLS,"is_booked="+status,null,null,null,null);

    }


    public Cursor findPatient(long id){
        return db.query(Patient.TABLE_NAME,Patient.COLS,"_id="+id,null,null,null,null);
    }

    public boolean setPatientStatus(long patientId,boolean isBooked){

        ContentValues values= new ContentValues();
        if(isBooked)
            values.put("is_booked",1);
        else
            values.put("is_booked",0);

        return db.update(Patient.TABLE_NAME,values,"_id="+patientId,null)>0;
    }

    public void openVisit(long patientId){
        setPatientStatus(patientId,true);
    }




    public boolean closeVisit(long visitId){

        Cursor cursor=findVisit(visitId);
        cursor.moveToFirst();
        long patientId=cursor.getLong(2);
        ContentValues values= new ContentValues();
        values.put("status",1);
        db.update(Visit.TABLE_NAME,values,"_id="+visitId,null);
        return setPatientStatus(patientId,false);

    }

    public Cursor getItems(){
        return db.query(Item.TABLE_NAME,Item.COLS,null,null,null,null,null);
    }

    public Cursor findItem(long id){
        return db.query(Item.TABLE_NAME,Item.COLS,"_id="+id,null,null,null,null);
    }

    public Cursor findItem(String constraints){
        return db.query(Item.TABLE_NAME,Item.COLS,"name LIKE "+"'%"+constraints+"%'",null,null,null,null);
    }


    public Cursor getDiagnosis(){
        return db.query(Diagnosis.TABLE_NAME,Diagnosis.COLS,null,null,null,null,null);

    }

    public Cursor findDiagnosis(long id){
        return db.query(Diagnosis.TABLE_NAME,Diagnosis.COLS,"_id="+id,null,null,null,null);
    }

    public Cursor findDiagnosis(String constraint){
        return  db.query(Diagnosis.TABLE_NAME,Diagnosis.COLS,"name LIKE "+"'%"+constraint+"%'",null,null,null,null);
    }

    public Cursor getPaymentMethods(){
        return db.query(PaymentMethod.TABLE_NAME,PaymentMethod.COLS,null,null,null,null,null);
    }

    public Cursor findPaymentMethod(long id){

        return db.query(PaymentMethod.TABLE_NAME,PaymentMethod.COLS,"_id="+id,null,null,null,null);
    }

    public Cursor getVisits(){
        return db.query(Visit.TABLE_NAME, Visit.COLS,null,null,null,null,null);
    }

    public Cursor findVisit(long id){
        return db.query(Visit.TABLE_NAME,Visit.COLS,"_id="+id,null,null,null,null);
    }

    public Cursor fetchIncompleteVisits(){
        return db.query(Visit.TABLE_NAME,Visit.COLS,"status=0",null,null,null,null);
    }

    public Cursor fetchVisitItems(long visitId){
        return db.query(PatientItem.TABLE_NAME,PatientItem.COLS,"visit_id="+visitId,null,null,null,null);

    }

    public Cursor fetchSyncPendingVisitItems(long visitId){
        return db.query(PatientItem.TABLE_NAME,PatientItem.COLS,"visit_id="+visitId,null,null,null,null);
    }

    public boolean deletePatientDiagnosis(long visitId,long id){

       return db.delete(PatientDiagnosis.TABLE_NAME,"visit_id="+visitId+" and diagnosis_id="+id,null)>0;
    }

    public boolean deletePatientItem(long visitId,long id){
        return db.delete(PatientItem.TABLE_NAME,"visit_id="+visitId+" and item_id="+id,null)>0;
    }

    public Cursor findVisitItem(long visitId,long itemId){
        return db.query(PatientItem.TABLE_NAME,PatientItem.COLS,"item_id="+itemId+" and visit_id="+visitId,null,null,null,null);

    }





    public long addVisitItem(PatientItem item){
        //first check whether that item already exists

        Cursor existingItem=findVisitItem(item.getVisit_id(),item.getItemId());
        if(existingItem.getCount()>0){
            Log.d("Item exists","Item Exists");
            //up
            existingItem.moveToFirst();

            int quantity=existingItem.getInt(existingItem.getColumnIndexOrThrow("quantity"));
            long id=existingItem.getLong(existingItem.getColumnIndexOrThrow("_id"));
            ContentValues values = new ContentValues();
            values.put("quantity",quantity+item.getQuantity());
            long result=db.update(PatientItem.TABLE_NAME,values,"_id="+id,null);
            if(result>0)
                return id;
            return -1;
        }
        else{
            Log.d("Item exists not","Item Exists not");
            return db.insert(PatientItem.TABLE_NAME,null,item.toContentValues());
        }


    }

    public Cursor getVisitPayments(long visitId){

        return db.query(PaymentView.TABLE_NAME,PaymentView.COLS,"visit_id="+visitId,null,null,null,null);

    }

    public boolean deletePayment(long Id){

        return db.delete(PaymentView.TABLE_NAME,PaymentView.COLS[0]+"="+Id,null)>0;
    }

    public long addPatientDiagnosis(PatientDiagnosis diagnosis){
        long itemId= diagnosis.getDiagnosis_id();
        Cursor cursor= db.query(PatientDiagnosis.TABLE_NAME,PatientDiagnosis.COLS,"visit_id="+diagnosis.getVisit_id()+" and diagnosis_id="+itemId,null,null,null,null);
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            return cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        }
        return db.insert(PatientDiagnosis.TABLE_NAME,null,diagnosis.toContentValues());
    }

    public Cursor findPatientDiagnosis(long visitId){
        return db.query(PatientDiagnosis.TABLE_NAME,PatientDiagnosis.COLS,"visit_id="+visitId,null,null,null,null);
    }

    public Cursor findSyncPendingPatientDiagnosis(long visitId){
        return db.query(PatientDiagnosis.TABLE_NAME,PatientDiagnosis.COLS,"visit_id="+visitId+" and sync_status=0",null,null,null,null);
    }

    public Cursor fetchSyncPendingPatients(){
        return db.query(Patient.TABLE_NAME,Patient.COLS,"sync_status=0",null,null,null,null);
    }

    public Cursor fetchSyncPendingVisits(){
        return db.query(Visit.TABLE_NAME, Visit.COLS,"sync_status=0 and status=1",null,null,null,null);

    }

    public long getRealPatientId(long visitId){

        Cursor cursor =findVisit(visitId);
        cursor.moveToFirst();
        long localId=cursor.getLong(cursor.getColumnIndexOrThrow("patient_id"));
        cursor.close();
        Cursor pCursor=findPatient(localId);
        pCursor.moveToFirst();
        return pCursor.getLong(pCursor.getColumnIndexOrThrow("id"));
    }

    public boolean setPatientSynced(long localId,long serverId){
        ContentValues values= new ContentValues();
        values.put("id",serverId);
        values.put("sync_status",1);

        long result= db.update(Patient.TABLE_NAME,values,"_id="+localId,null);
        return result>0;
    }

    public boolean setVisitSynced(long localId,long serverId){
        ContentValues values= new ContentValues();
        values.put("id",serverId);
        values.put("sync_status",1);
        long result=db.update(Visit.TABLE_NAME,values,"_id="+localId,null);
        return  result>0;

    }

    public Report fetchDailyReport(String date){
        Report report= new Report();
        report.setDate(date);
        //fetch all visits pertaining to the day

        Cursor visitsCursor=db.query(Visit.TABLE_NAME,new String[]{"_id","created_at"},"created_at='"+date+"'",null,null,null,null);
        int count =visitsCursor.getCount();
        report.setTotalVisits(count);
        if(count >0){
            double total=0;
            while(visitsCursor.moveToNext()){
                long visitId=visitsCursor.getLong(visitsCursor.getColumnIndexOrThrow("_id"));
                Cursor payCursor=getVisitPayments(visitId);

                while(payCursor.moveToNext()){
                    total+=payCursor.getDouble(payCursor.getColumnIndexOrThrow("amount"));
                }
                payCursor.close();
                report.setIncome(total);
            }
        }
        visitsCursor.close();
        return  report;
    }


















}
