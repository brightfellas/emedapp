package ke.co.brightfellas.emed;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Report;

/**
 * Created by iUwej on 9/14/14.
 */
public class ReportsFragment extends Fragment {

    private Report report;
    private WebView webView;
    private Button btnSearch;
    private EditText txtDate;
    private ProgressDialog dialog;
    private DbAdapter db;
    private Calendar myCalendar;




    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.frament_reports,container,false);
        webView= (WebView)view.findViewById(R.id.txt_report_view);
        btnSearch=(Button)view.findViewById(R.id.btn_search);
        txtDate=(EditText)view.findViewById(R.id.txt_date_filter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        db= new DbAdapter(getActivity());
        btnSearch.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String filter=txtDate.getEditableText().toString();
                if(!filter.isEmpty()){
                        new LoadReport().execute(filter);
                }
                else{
                    Toast.makeText(getActivity(),"Date field cannot be empty",Toast.LENGTH_SHORT).show();
                }
            }
        });
        String strDate= new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        new LoadReport().execute(strDate);
        myCalendar=Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {

                myCalendar.set(Calendar.YEAR,year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateTextField();


            }
        };

        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dialog= new DatePickerDialog(getActivity(),date,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });
    }

    private void updateTextField() {
        String strDate= new SimpleDateFormat("yyyy-MM-dd").format(myCalendar.getTime());
        txtDate.setText(strDate);


    }

    class LoadReport extends AsyncTask<String,Void, Report>{

        @Override
        protected Report doInBackground(String... strings) {
            String filter= strings[0];
            db.open();
            Report report1= db.fetchDailyReport(filter);
            db.close();

            return report1;
        }

        @Override
        protected void onPostExecute(Report report) {
            dialog.dismiss();
            if(report !=null){
                webView.loadData(report.toHtml(),"text/html",null);
            }
            else{
                webView.loadData("Error loading report","text/html",null);
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog= ProgressDialog.show(getActivity(),"Reports","calculating summary....");
        }
    }
}
