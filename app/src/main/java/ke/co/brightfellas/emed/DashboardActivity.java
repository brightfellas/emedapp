package ke.co.brightfellas.emed;


import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by iUwej on 8/16/14.
 */
public class DashboardActivity  extends ActionBarActivity implements RegisterPatientFragment.OnItemAddedListener,PatientsFragment.OnPatientNavigationListener{

    private DrawerLayout drawerLayout;
    private ListView lstView;
    private ActionBarDrawerToggle mDrawerToggle;
    EmedApplication app;
    public static final String AUTHORITY="ke.co.brightfellas.emed";
    public static final String ACCOUNT="emedapp";
    public static final String ACCOUNT_TYPE="emed.co.ke";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        app=(EmedApplication)getApplication();
        drawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);
        lstView=(ListView)findViewById(R.id.lst_view);
        mDrawerToggle= new ActionBarDrawerToggle(this,drawerLayout,R.drawable.apptheme_ic_navigation_drawer,R.string.drawer_open,R.string.drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        ActionBar actionBar=getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        initializeDrawer();
        //Toast.makeText(this,"Items fetched "+app.getItems().size(),Toast.LENGTH_LONG).show();
        if(savedInstanceState==null){
           showFragment(R.id.patients_fragment,null);

        }


    }

    private void initializeDrawer(){

        String[] items={"Patients","New Patient","Billing","Reports","About","Help"};
        int count=0;
        final ArrayAdapter<DrawerItem> adapter = new ArrayAdapter<DrawerItem>(this,android.R.layout.simple_list_item_1,new ArrayList<DrawerItem>());
        lstView.setAdapter(adapter);
        for(String item: items){
            adapter.add(new DrawerItem(count++,item));
        }
        adapter.notifyDataSetChanged();
        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DrawerItem item= adapter.getItem(i);
                switch (item.title){
                    case "New Patient":
                        showFragment(R.id.new_patients_fragment,null);
                        drawerLayout.closeDrawers();
                        break;
                    case "Patients":
                        showFragment(R.id.patients_fragment,null);
                        drawerLayout.closeDrawers();
                        break;
                    case "Billing":
                       showFragment(R.id.visits_fragment,null);
                        drawerLayout.closeDrawers();
                        break;
                    case "Reports":
                         showFragment(R.id.reports_fragment,null);
                        drawerLayout.closeDrawers();
                        break;


                    default:
                        drawerLayout.closeDrawers();
                        return;
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(mDrawerToggle.onOptionsItemSelected(item))
            return true;

        switch (item.getItemId()){

            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public void itemAdded(long Id) {

       showFragment(R.id.patients_fragment,null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard,menu);


        return true;
    }



    public void showFragment(int id,@Nullable Bundle params){
        Fragment fragment=null;
        switch(id){
            case R.id.patients_fragment:
                fragment=new PatientsFragment();
                if(params !=null)
                    fragment.setArguments(params);
                 FragmentManager fm = getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                setTitle("Patients");
                break;
            case R.id.reports_fragment:
                fragment= new ReportsFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack("f").commit();
                setTitle("Reports");
                break;

            case R.id.new_patients_fragment:
                fragment= new RegisterPatientFragment();
                if(params !=null)
                    fragment.setArguments(params);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack("f").commit();
                setTitle("New Patient");
                break;
            case R.id.procedures_fragment:
                fragment= new FragmentSelectItems();
                if(params !=null)
                    fragment.setArguments(params);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack("f").commit();
               setTitle("Drugs & Procedures");
                break;
            case R.id.diagnosis_fragment:
                fragment= new DiagnosisFragment();
                if(params !=null)
                    fragment.setArguments(params);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack("f").commit();
                setTitle("Diagnosis");
                break;
            case R.id.visits_fragment:
                fragment= new VisitFragment();
                if(params !=null)
                    fragment.setArguments(params);
                getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).addToBackStack("f").commit();
                setTitle("Billing");
                break;
            case R.id.payment_fragment:
                fragment= new PaymentFragment();
                if(params !=null)
                    fragment.setArguments(params);
                getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).addToBackStack("f").commit();
                setTitle("Payment");
                break;
            default:
                Toast.makeText(getApplication(),"Fragment not available",Toast.LENGTH_SHORT).show();









        }
    }

    @Override
    public void navigate(long patientId, long visitId ) {

        showFragment(R.id.visits_fragment,null);
    }


}
