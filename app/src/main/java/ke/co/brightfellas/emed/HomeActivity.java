package ke.co.brightfellas.emed;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Diagnosis;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Item;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Patient;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.PaymentMethod;


public class HomeActivity extends ActionBarActivity  implements ActivationFragment.OnActivationCompleteListener, LoginFragment.OnLoginCompleteListener{

    private EmedApplication app;
    private ProgressDialog progressDialog;

    public static final String AUTHORITY="ke.co.brightfellas.emed";
    public static final String ACCOUNT_TYPE="emed.co.ke";
    public static final String ACCOUNT="emedapp";
    Account account;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        app= (EmedApplication) getApplication();
        account=CreateSyncAccount(this);
        if(savedInstanceState==null){
            if(app.getAppIsActivated()){
                getSupportFragmentManager().beginTransaction().replace(R.id.container,new LoginFragment()).commit();

            }
            else{
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new ActivationFragment()).commit();

            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Create a dummy account for the SyncAdapter
     *
     * */
    public static Account CreateSyncAccount(Context context){

        //create the account type and the default account
        Account newAccount= new Account(ACCOUNT,ACCOUNT_TYPE);

        //get an instance of the android account manager
        AccountManager  accountManager= (AccountManager)context.getSystemService(Context.ACCOUNT_SERVICE);
        //add the account and the account type returning the newly created account otherwise report and error
        if(accountManager.addAccountExplicitly(newAccount,null,null)){
            ContentResolver.setSyncAutomatically(newAccount,AUTHORITY,true);
            return  newAccount;
        }

        else{
            //the account exists or some error occured
           return null;
        }

    }









    @Override
    public void onActivated(String code) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container,new LoginFragment()).commit();

    }

    @Override
    public void loginComplete() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container,new InstantiateFragement()).commit();

    }
}
