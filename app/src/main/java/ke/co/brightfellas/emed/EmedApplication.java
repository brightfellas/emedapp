package ke.co.brightfellas.emed;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by iUwej on 8/19/14.
 */

@ReportsCrashes(
        formKey = "",
        mailTo = "jmunene@brightfellas.co.ke",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast
)
public class EmedApplication extends Application {

    private RequestQueue requestQueue;





    private Credential credential;




    public EmedApplication(){


    }

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);

    }

    public Credential getCredential() {
        return new Credential(getAccessToken(),getDbName());
    }

    public void setCredential(Credential credential) {
        setAccessToken(credential.getToken());
        saveDbName(credential.getDbname());
    }


    public RequestQueue getRequestQueue(){
        if(requestQueue==null)
            requestQueue=Volley.newRequestQueue(this);
        return requestQueue;
    }

    public void saveSetting(String key,String value,Context context){
        final SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor= prefs.edit();
        editor.putString(key,value);
        editor.commit();

    }

    public void setUsername(String username){
        saveSetting("username",username,this);
    }

    public void setPassword(String password){
        saveSetting("password",password,this);
    }


    public void setAccessToken(String token){
        saveSetting("access_token",token,this);
    }

    public String getAccessToken(){
        final SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(this);
        return prefs.getString("access_token","");
    }

    public boolean isInitialized(){
        final SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(this);
        return prefs.getBoolean("initialized",false);

    }

    public void setAppIsInitialized(boolean isInitialized){
        final SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor=prefs.edit();
        editor.putBoolean("initialized", isInitialized);
        editor.commit();
    }

    public void saveDbName(String dbName){
        final SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor=prefs.edit();
        editor.putString("db_name",dbName);
        editor.commit();
    }

    public String getDbName(){
        final SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(this);
        return prefs.getString("db_name",null);
    }

    public boolean getAppIsActivated(){
        return  getDbName() !=null;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        //db.close();
    }
}
