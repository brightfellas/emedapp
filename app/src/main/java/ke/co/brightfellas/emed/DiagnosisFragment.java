package ke.co.brightfellas.emed;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Diagnosis;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.PatientDiagnosis;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.PaymentView;

/**
 * Created by iUwej on 8/23/14.
 */
public class DiagnosisFragment extends ListFragment {

    private  EmedApplication app;

    private SimpleCursorAdapter cursorAdapter;
    private AutoCompleteTextView txtSearch;
    private Button btnAdd;
    private  Diagnosis selected=null;
    private ArrayAdapter<Diagnosis> selectedDiagnosis;

    private long visitId;

    private ProgressDialog progressDialog;
    private Button btnSave;
    private DbAdapter db;
    interface OnDiagnosisAddedListener{
        public void diagnosisAdded(long visitId);

    }

    OnDiagnosisAddedListener navListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        navListener=(OnDiagnosisAddedListener)activity;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        app=(EmedApplication)getActivity().getApplication();
        db=new DbAdapter(getActivity());
        registerForContextMenu(getListView());
        if(savedInstanceState==null){

            visitId=getArguments().getLong("visit_id");
        }
        else{
            visitId=savedInstanceState.getLong("visit_id");

        }
        cursorAdapter= new SimpleCursorAdapter(getActivity(),R.layout.two_item_list_template,null,new String[]{"name"},new int[]{R.id.txt_header});
        cursorAdapter.setCursorToStringConverter( new SimpleCursorAdapter.CursorToStringConverter() {
            @Override
            public CharSequence convertToString(Cursor cursor) {
                final int columnIndex=cursor.getColumnIndexOrThrow("name");
                return cursor.getString(columnIndex);
            }
        });

        cursorAdapter.setFilterQueryProvider( new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence charSequence) {
                String constraints=charSequence==null?null: charSequence.toString();
                return db.findDiagnosis(constraints);
            }
        });
        txtSearch.setAdapter(cursorAdapter);
        txtSearch.setThreshold(1);
        txtSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               Cursor c=(Cursor)cursorAdapter.getItem(i);
                selected=Diagnosis.createDiagnosisFromCursor(c);
            }
        });

        selectedDiagnosis= new ArrayAdapter<Diagnosis>(getActivity(),android.R.layout.simple_list_item_1,new ArrayList<Diagnosis>());
        setListAdapter(selectedDiagnosis);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selected !=null){
                    selectedDiagnosis.add(selected);
                    selectedDiagnosis.notifyDataSetChanged();
                    txtSearch.setText("");
                }
                else{
                    Toast.makeText(getActivity(),"You must select a diagnosis",Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               new Handler().post(new Runnable() {
                   @Override
                   public void run() {
                       saveToDb();
                       navListener.diagnosisAdded(visitId);
                   }
               });

            }
        });
        getListView().setDivider(getResources().getDrawable(R.drawable.black_background));
        getListView().setDividerHeight(1);
        new FetchData().execute();

    }

    private void saveToDb(){

        progressDialog=ProgressDialog.show(getActivity(),"Emed","saving...");

        for(int i=0,count=selectedDiagnosis.getCount();i<count;i++){

            Diagnosis diagnosis=selectedDiagnosis.getItem(i);
            PatientDiagnosis p= new PatientDiagnosis(visitId,diagnosis.getId());
            long id=db.addPatientDiagnosis(p);
            Log.d("Added Patient diagnosis with id: ",id+"");
        }

        progressDialog.dismiss();

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater= getActivity().getMenuInflater();
        inflater.inflate(R.menu.edit_items,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info=(AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        Diagnosis view=selectedDiagnosis.getItem(info.position);
        switch(item.getItemId()){
            case R.id.remove:
                long id=view.getId();
                db.open();
                db.deletePatientDiagnosis(visitId,id);
                selectedDiagnosis.remove(view);
                selectedDiagnosis.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("visit_id",visitId);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_diagnosis,container,false);
        //txtQuantity=(EditText)view.findViewById(R.id.txt_quantity);
        txtSearch=(AutoCompleteTextView)view.findViewById(R.id.txt_search_diagnosis);
        btnAdd=(Button)view.findViewById(R.id.btn_add);
        btnSave=(Button)view.findViewById(R.id.btn_save);
        return view;
    }

    class LoadVisitDiagnosis extends AsyncTask<Long,Void,List<Diagnosis>>{
        @Override
        protected List<Diagnosis> doInBackground(Long... longs) {
            db.open();
            Cursor cursor= db.findPatientDiagnosis(longs[0]);
            if(cursor.getCount()>0){
                ArrayList<Diagnosis> list= new ArrayList<>();
                while(cursor.moveToNext()){
                    long diagnosisId=cursor.getLong(cursor.getColumnIndexOrThrow("diagnosis_id"));
                    Cursor diag= db.findDiagnosis(diagnosisId);
                    diag.moveToFirst();
                    list.add(Diagnosis.createDiagnosisFromCursor(diag));
                }

                return list;
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Diagnosis> diagnosises) {
           progressDialog.dismiss();
            if(diagnosises !=null){
                for(Diagnosis d: diagnosises){
                    selectedDiagnosis.add(d);
                }
                selectedDiagnosis.notifyDataSetChanged();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog= ProgressDialog.show(getActivity(),"Emed","loading diagnosis...");
        }
    }



    class FetchData extends AsyncTask<Void,Void,Cursor>{
        @Override
        protected Cursor doInBackground(Void... voids) {
            db.open();
            return db.getDiagnosis();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=ProgressDialog.show(getActivity(),"Emed","loading diagnosis...");
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            progressDialog.dismiss();
            if(cursor !=null){
                cursorAdapter.changeCursor(cursor);
                cursorAdapter.notifyDataSetChanged();
            }
            new LoadVisitDiagnosis().execute(visitId);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(db !=null){
            db.open();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(db !=null){
            db.close();
        }
    }
}
