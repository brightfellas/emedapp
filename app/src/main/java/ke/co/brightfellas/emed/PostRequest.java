package ke.co.brightfellas.emed;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;

/**
 * Created by iUwej on 8/21/14.
 */
public class PostRequest<T> extends JsonRequest<T> {

    public PostRequest(String  item,String url,Response.Listener<T> listener,Response.ErrorListener errorListener){
        super(Method.POST,url,item,listener,errorListener);
    }


    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {

        if(!(response.statusCode==200))
            return Response.error(new VolleyError(response));

        return null;
    }
}
