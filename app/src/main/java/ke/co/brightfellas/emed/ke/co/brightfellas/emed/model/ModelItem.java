package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import ke.co.brightfellas.emed.SkipSerialization;

/**
 * Created by iUwej on 9/4/14.
 */
abstract class ModelItem {

    abstract  String getCreateSQL();
    abstract String getDropSQL();
    abstract ContentValues toContentValues();

    @SkipSerialization
    protected long sync_status;

    public long getSync_status() {
        return sync_status;
    }

    public void setSync_status(long sync_status) {
        this.sync_status = sync_status;
    }

    public  void onCreate(SQLiteDatabase db){
        db.execSQL(getCreateSQL());
    }

    public void onUpgrade(SQLiteDatabase db, int newVersion, int oldVersion){
        db.execSQL(getDropSQL());
        onCreate(db);
    }

}
