package ke.co.brightfellas.emed;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ComponentName;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Patient;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Visit;

/**
 * Created by iUwej on 9/8/14.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    interface OnPatientsSyncedListener{

        public void onPatientsSnyced();
    }

    private  OnPatientsSyncedListener patientsSyncedListener= new OnPatientsSyncedListener() {
        @Override
        public void onPatientsSnyced() {

            uploadVisits();
        }
    };

    private void uploadVisits() {
        List<String> visits= getVisits();
        if(visits.size()<1){
            Log.d("Visit Syncs","No pendind visits");
            return;
        }
        else{
            Log.d("Visit Syncs",visits.size()+" visits");
        }


        for(String json: getVisits()){
            String url="http://lite.emed.co.ke/api/v1/visits?api_token="+credential.getToken()+"&dbname="+credential.getDbname();
            try {
                JSONObject jsonObject= new JSONObject(json);
                JsonObjectRequest request= new JsonObjectRequest(Request.Method.POST,url,jsonObject,visitListener,visitUploadErrorListener);
                emedSingleton.addToRequestQueue(request);
            } catch (JSONException e) {
                Log.e("Error Parsing Json", e.getMessage());
            }
        }
    }

    DbAdapter db;
    private EmedApplication app;
    private int patientsCount,patientSynced;
    private  Credential credential;
    private  EmedSingleton emedSingleton;

    //set up the sync adapter;
    public SyncAdapter(Context context,boolean autoInitialize){
        super(context,autoInitialize);
        db= new DbAdapter(getContext());

        emedSingleton=EmedSingleton.getInstance(getContext());
        db.open();

    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public SyncAdapter(Context context,boolean autoInitialize,boolean allowParallelSyc){
        super(context,autoInitialize,allowParallelSyc);
        db= new DbAdapter(context);
    }

    private Response.ErrorListener patientUploadErrorListener= new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

            //create a notification indicating error upload a patient
            notifyUser("Emed Sync","Error syncing emed account",1,0);
            Log.d("Error while syncing patients",error.getMessage()+"");
            if(error.networkResponse !=null){
                Log.d("Network status",error.networkResponse.statusCode+"");
            }
            patientSynced++;
            if(patientSynced==patientsCount){

                    patientsSyncedListener.onPatientsSnyced();

            }

        }
    };

    private Response.ErrorListener visitUploadErrorListener= new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            //create a notification indicating of an error trying to upload visit
            notifyUser("Visits Sync","Error Syncing visit",0,1);
            Log.d("Error Syncing Visit",error.getMessage());
        }
    };


    private Response.Listener<JSONObject> patientListener= new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d("Patient Sync","some success"+response.toString());
            try{
                JSONObject jsonObject=response.getJSONObject("message");
                long localId=jsonObject.getLong("_id");
                JSONObject patientObject=jsonObject.getJSONObject("patient");
                long serverId=patientObject.getLong("id");

                if(db.setPatientSynced(localId,serverId))
                    Log.d("Patient:","localId:"+localId+",ServerId: "+serverId);
                else
                    Log.d("Emed Sync","Failed to update db");
                patientSynced++;
                if(patientSynced==patientsCount){
                    Log.d("Patients synced:",patientSynced+"");
                    patientsSyncedListener.onPatientsSnyced();

                }
            }
            catch (JSONException e){
                Log.d("Error Parsing Json",e.getMessage());
            }

        }
    };



    private Response.Listener<JSONObject> visitListener= new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {

            try {
                JSONObject object= response.getJSONObject("message");
                long localId=object.getLong("_id");
                JSONObject visit= object.getJSONObject("patient");
                long serverId=visit.getLong("id");
               if(db.setPatientSynced(localId,serverId)){
                   uploadItems(localId, serverId);

                   //also post the diagnosis here
                   uploadDiagnosis(localId,serverId);

                   //upload the payments here
                   uploadPayments(localId,serverId);

               }
                else{
                   Log.d("Error updating databased","LocalId: "+localId+" ServerId: "+serverId);
               }
            } catch (JSONException e) {
               Log.e("Error parsing data",e.getMessage());
            }

        }
    };

    private void uploadItems(long localId, long serverId)  {
        Log.d("Visits Sync", "LocalId: " + localId + " ServerId: " + serverId);

        String items= getItems(localId,serverId);
        if(items==null){
            Log.d("No items to Sync","Nill items to sync");
            return;
        }
        Log.d("Posting Visit Items",items);
        JSONObject iObject= null;
        try {
            iObject = new JSONObject(items);
            String url="http://lite.emed.co.ke/api/v1/procedures_drugs?api_token="+credential.getToken()+"&dbname="+credential.getDbname();
            JsonObjectRequest request= new JsonObjectRequest(Request.Method.POST,url,iObject,postItemsListener,postItemsErrorListener);
            emedSingleton.addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void uploadPayments(long localId,long serverId){

        Cursor cursor= db.getVisitPayments(localId);
        int cursorCount=cursor.getCount();
        if(cursorCount<1){
            Log.d("Syncing Payments","No Payments to Sync");
            return;
        }

        int count =0;
        StringBuffer data= new StringBuffer();
        data.append("{\"items\":[");
        while(cursor.moveToNext()){

            String json=String.format("{\"paymenttype_id\":%d,\"amount\":%.2f,\"description\":\"Amount paid\"}",cursor.getLong(cursor.getColumnIndexOrThrow("method_id")),cursor.getDouble(cursor.getColumnIndexOrThrow("amount")));
            data.append(json);
            if(count !=cursorCount-1){
                data.append(",");
            }
            ++count;
        }
        data.append("],");
        data.append("\"visit_id\":"+serverId);
        data.append("}");

        try {
            JSONObject payObject= new JSONObject(data.toString());
            String url="http://lite.emed.co.ke/api/v1/payments?api_token="+credential.getToken()+"&dbname="+credential.getDbname();
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url,payObject,new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.i("Payment Sync",response.toString());

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String msg=error.getMessage() !=null?error.getMessage():"Error Posting Payments";
                    Log.e("Error Posting Payments",msg);

                }
            });

            emedSingleton.addToRequestQueue(request);

        } catch (JSONException e) {
           Log.d("Error syncing payments",e.getMessage());
        }
    }

    private void uploadDiagnosis(long localId,long serverId){
        Cursor cursor =db.findPatientDiagnosis(localId);
        long patientId=db.getRealPatientId(localId);
        StringBuffer data= new StringBuffer();
        data.append("{\"diagnosis_id\":[");
        int count=0;
        int cursorCount=cursor.getCount();
        if(cursorCount==0){
            Log.d("Sync Operation","No Diagnosis to sync");
            return;
        }
        while(cursor.moveToNext()){
            long diagnosisId= cursor.getLong(cursor.getColumnIndexOrThrow("diagnosis_id"));
            data.append(diagnosisId);
            if(count !=cursorCount-1)
                data.append(",");
            ++count;
        }
        data.append("],");
        data.append("\"patient_id\":"+patientId);
        data.append(",");

        data.append("\"visit_id\":"+serverId);

        data.append("}");
        Log.e("Diagnosis Sync Data",data.toString());

        try {
            JSONObject object = new JSONObject(data.toString());
            String url="http://lite.emed.co.ke/api/v1/consultations?api_token="+credential.getToken()+"&dbname="+credential.getDbname();
            JsonObjectRequest request= new JsonObjectRequest(Request.Method.POST,url,object,new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.d("Diagnosis Sync response",response.toString());

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(error.getMessage() !=null){
                        Log.e("Diagnosis Sync",error.getMessage());
                    }
                    else
                        Log.e("Diagnosis Sync","Error Syncing diagnosis");
                }
            });

            emedSingleton.addToRequestQueue(request);
        } catch (JSONException e) {
          Log.d("Diagnosis Serialization",e.getMessage());
        }

    }

    private void login(){

        final Response.Listener<JSONObject> loginResponse= new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String access_token=response.getString("token");
                    credential= new Credential(access_token,AppConfig.getDbName(getContext()));
                    updatePatients();

                } catch (JSONException e) {
                    Log.e("Error Parsing",e.getMessage());
                }
            }
        };

        final Response.ErrorListener errorListener= new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error.getMessage() !=null){
                    Log.d("Error login in",error.getMessage());
                }
                else{
                    Log.d("Error login in","Login Error");
                }


            }
        };
        String url="http://lite.emed.co.ke/login?username="+AppConfig.getUsername(getContext())+"&password="+AppConfig.getPassword(getContext())+"&dbname="+AppConfig.getDbName(getContext());

        JsonObjectRequest loginRequest= new JsonObjectRequest(Request.Method.POST,url,null,loginResponse,errorListener);
        emedSingleton.addToRequestQueue(loginRequest);

    }


    private List<String> getPatients(){
        Cursor cursor= db.fetchSyncPendingPatients();
        Log.i("Cursor Size",cursor.getCount()+" patients");
        ArrayList<String> patients= new ArrayList<String>();
        while(cursor.moveToNext()){
            patients.add(Patient.toJson(cursor));

        }
        patientsCount=patients.size();
        return patients;
    }

    public List<String> getVisits(){

        List<String> jsonVisits= new ArrayList<>();
        Cursor cursor=db.fetchSyncPendingVisits();
        while(cursor.moveToNext()){

            String json=String.format("{\"patient_id\": %d,\"_id\":%d,\"status\":%d}",db.getRealPatientId(cursor.getLong(0)),cursor.getLong(0),cursor.getLong(5));
            jsonVisits.add(json);
        }
        cursor.close();

        return jsonVisits;
    }

    private Response.Listener<JSONObject> postItemsListener= new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            //set the sync status of the items to 1

            Log.d("Items Response",response.toString());

        }
    };

    private Response.ErrorListener postItemsErrorListener= new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            String msgError=(error.getMessage() !=null )? error.getMessage(): "Error saving items";
            Log.d("Error Syncing Items",msgError);
        }
    };

    public String getItems(long visitId,long serverId){
        Cursor cursor=db.fetchSyncPendingVisitItems(visitId);
        StringBuffer items= new StringBuffer();
        int count =0;
        int cursorCount=cursor.getCount();
        //add the first items
        if(cursorCount<1)
            return null;
        items.append("{\"items\":[");


        while(cursor.moveToNext()){
            int itemColumn=cursor.getColumnIndexOrThrow("item_id");
            long itemId=cursor.getLong(itemColumn);
            Cursor item= db.findItem(itemId);
            item.moveToFirst();
            String json=String.format("{\"item_id\":%d,\"units\":%d,\"price\":%.2f}",itemId,cursor.getInt(1),item.getDouble(4));
            items.append(json);
            if(count != cursorCount-1)
                items.append(",");
            item.close();
            ++count;
        }

        items.append("],");
        items.append("\"visit_id\":" + serverId);
        items.append("}");



        return  items.toString();

    }

    public List<String> getDiagnosis(long visitId){

        return null;
    }

    public void notifyUser(String title,String message, int id, int number){

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext());
       builder.setContentTitle(title)
               .setNumber(number)
               .setContentText(message);
        Intent resultIntent= new Intent(getContext(),HomeActivity.class);
        TaskStackBuilder stackBuilder=TaskStackBuilder.create(getContext());
        stackBuilder.addParentStack(HomeActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent=stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);
        NotificationManager nManager=(NotificationManager)getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(id,builder.build());



    }












    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        patientSynced=0;

        //sync all the patients
        Log.i("Initalizing Sync","Sync Initiliazed");
        login();


    }

    private void updatePatients() {
        List<String> patients=getPatients();
        if(patients.size()==0){
            patientsSyncedListener.onPatientsSnyced();
        }
        Log.i("Emed Sync", patients.size() + " patients");
        for(String patient: patients){


            try{
                Log.d("Patient:",patient);
                JSONObject jsonObject= new JSONObject(patient);
                JsonObjectRequest request= new JsonObjectRequest(Request.Method.POST,"http://lite.emed.co.ke/api/v1/patients?api_token="+credential.getToken()+"&dbname="+credential.getDbname(),jsonObject,patientListener,patientUploadErrorListener);
                emedSingleton.addToRequestQueue(request);
            }
            catch (JSONException e){
                Log.d("Error Parsing Json",e.getMessage());
            }


        }
    }


}
