package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.orm.SugarRecord;

import ke.co.brightfellas.emed.SkipSerialization;

/**
 * Created by iUwej on 8/19/14.
 */
public class Patient extends ModelItem {

    private String fname;
    private String lname;
    private String gender;
    private String date_of_birth;
    private String dependant;
    private String mobileno;
    private String id_number;
    private long id;
    private long _id;

    @SkipSerialization
    private long is_booked;

    public long getIs_booked() {
        return is_booked;
    }

    public void setIs_booked(long is_booked) {
        this.is_booked = is_booked;
    }

    public long get_id() {
        return _id;
    }

    public ContentValues toContentValues(){
        ContentValues values= new ContentValues();
        values.put("fname",fname);
        values.put("lname",lname);
        values.put("gender",gender);
        values.put("date_of_birth",date_of_birth);
        values.put("dependant",dependant);
        values.put("mobileno",mobileno);
        values.put("id_number",id_number);
        values.put("id",id);
        values.put("sync_status",sync_status);
        return values;
    }


    public long getId() {
        return id;
    }

    //for internal record keeping
    private long patient_no; //for eternal record keeping(Why I had no option)

    public Patient(){}

    public Patient(String fname, String lname, String gender, String date_of_birth, String dependant, String mobileno, String id_number) {
        this.fname = fname;
        this.lname = lname;
        this.gender = gender;
        this.date_of_birth = date_of_birth;
        this.dependant = dependant;
        this.mobileno = mobileno;
        this.id_number = id_number;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getDependant() {
        return dependant;
    }

    public void setDependant(String dependant) {
        this.dependant = dependant;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    @Override
    public String toString() {
        return fname+" "+lname;
    }

    public long getPatient_no() {
        return patient_no;
    }

    public void setPatient_no(long patient_no) {
        this.patient_no = patient_no;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    @Override
    String getCreateSQL() {
        return "create table patients" +
                "(_id integer primary key autoincrement," +
                "id integer," +
                "fname text, " +
                "lname text," +
                "gender text," +
                "date_of_birth text," +
                "dependant text," +
                "mobileno text," +
                "id_number text," +

                "sync_status integer default 0," +
                "is_booked integer default 0);";
    }

    public static String toJson(Cursor cursor){

        String format="{\"_id\":%d,\"fname\":\"%s\",\"lname\":\"%s\",\"gender\":\"%s\",\"date_of_birth\":\"%s\",\"dependant\":\"%s\",\"mobileno\":\"%s\",\"id_number\":\"%s\"}";
        String json=String.format(format,cursor.getLong(cursor.getColumnIndexOrThrow("_id")),cursor.getString(cursor.getColumnIndexOrThrow("fname")),cursor.getString(cursor.getColumnIndexOrThrow("lname")),cursor.getString(cursor.getColumnIndexOrThrow("gender")),cursor.getString(cursor.getColumnIndexOrThrow("date_of_birth")),cursor.getString(cursor.getColumnIndexOrThrow("dependant")),cursor.getString(cursor.getColumnIndexOrThrow("mobileno")),cursor.getString(cursor.getColumnIndexOrThrow("id_number")));
        return json;

    }




    public static final String[] COLS={"_id","id","fname","lname","gender","date_of_birth","dependant","mobileno","id_number","sync_status","is_booked"};

    public Patient(long _id, long id,String f,String l, String g, String date, String dependant, String mobile,String id_number,int status){
        this(f,l,g,date,dependant,mobile,id_number);
        this._id=_id;
        this.id=id;
        this.sync_status=status;


    }




    public static Patient createFromCursor(Cursor cursor){
        cursor.moveToFirst();
        Patient patient= new Patient(cursor.getLong(0),cursor.getLong(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getInt(9));
        return patient;
    }

    @Override
    String getDropSQL() {
        return "drop table if exists patients";
    }

    public static final String TABLE_NAME="patients";


}
