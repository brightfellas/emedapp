package ke.co.brightfellas.emed;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Billing;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.PatientItem;

/**
 * Created by iUwej on 9/7/14.
 */
public class VisitBillLoader extends AsyncTaskLoader<Billing> {

    private DbAdapter db;
    private  long visitId;

    public VisitBillLoader(Context ctx,long visitId){
        super(ctx);
        db= new DbAdapter(getContext());
       this.visitId=visitId;
    }

    @Override
    public Billing loadInBackground() {
        db.open();
        double totalAmount=0;
        double amountPaid=0;

        //fetch all the patient items
        Cursor cursor=db.fetchVisitItems(visitId);
        while(cursor.moveToNext()){
            int quantity=cursor.getInt(1);
            long itemId=cursor.getLong(4);
            Cursor itemCursor=db.findItem(itemId);
            itemCursor.moveToFirst();
            double price=itemCursor.getDouble(4);
            totalAmount += (quantity * price);
        }

        //fetch all amount paid here and calculate the total
        Cursor paymentCursor=db.getVisitPayments(visitId);
        while(paymentCursor.moveToNext()){
            double paid=paymentCursor.getDouble(2);
            amountPaid+=paid;
        }

        return new Billing(visitId,totalAmount,amountPaid);
    }

}
