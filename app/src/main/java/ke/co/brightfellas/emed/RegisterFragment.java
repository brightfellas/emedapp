package ke.co.brightfellas.emed;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by iUwej on 8/16/14.
 */

public class RegisterFragment extends Fragment {

    interface OnClinicRegistered{

        public void onClinicRegister();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,  Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_register,container,false);
        return view;
    }


}
