package ke.co.brightfellas.emed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Item;

/**
 * Created by iUwej on 8/23/14.
 */
public class ItemsFragment extends ListFragment {
    ArrayAdapter<Item> itemsAdapter;
    private EmedApplication app;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        app= (EmedApplication) getActivity().getApplication();

        //itemsAdapter= new ArrayAdapter<Item>(getActivity(),android.R.layout.simple_list_item_1,app.getItems());
        setListAdapter(itemsAdapter);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list,container,false);
    }
}
