package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by iUwej on 8/19/14.
 */
public class Item extends  ModelItem  {



    public static final String[] COLS={"_id","name","description","instructions","price","itemtype_id","statuses_id","stocktype"};

    @SerializedName("id")
    private long _id;
    private  String name;
    private String description;
    private String instructions;
    private double price;
    private int itemtype_id;
    private int statuses_id;
    private String stocktype;

    public Item(long _id, String name, String description, String instructions, double price, int itemtype_id, int statuses_id, String stocktype) {
        this._id = _id;
        this.name = name;
        this.description = description;
        this.instructions = instructions;
        this.price = price;
        this.itemtype_id = itemtype_id;
        this.statuses_id = statuses_id;
        this.stocktype = stocktype;
    }

    public ContentValues toContentValues(){
        ContentValues cv=  new ContentValues();
        cv.put("_id",_id);
        cv.put("name",name);
        cv.put("description",description);
        cv.put("instructions",instructions);
        cv.put("price",price);
        cv.put("itemtype_id",itemtype_id);
        cv.put("statuses_id",statuses_id);
        cv.put("stocktype",stocktype);
        return cv;
    }

    public Item(){}

    public static  Item createFromCursor(Cursor cursor){
        Item item = new Item(cursor.getLong(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getDouble(4),cursor.getInt(5),cursor.getInt(6),cursor.getString(7));
        return item;
    }


    @Override
    String getCreateSQL() {
        return "create table items(_id integer primary key," +
                "name text not null," +
                "description text," +
                "instructions text," +
                "price double,"+
                "itemtype_id integer," +
                "statuses_id integer," +
                "stocktype text);";
    }

    @Override
    String getDropSQL() {
        return "drop table if exists items";
    }

    public static final String TABLE_NAME="items";




public  long getId(){
    return _id;
}

public void setId(int id){
    _id=id;
}




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getItemtype_id() {
        return itemtype_id;
    }

    public void setItemtype_id(int itemtype_id) {
        this.itemtype_id = itemtype_id;
    }

    public int getStatuses_id() {
        return statuses_id;
    }

    public void setStatuses_id(int statuses_id) {
        this.statuses_id = statuses_id;
    }

    public String getStocktype() {
        return stocktype;
    }

    public void setStocktype(String stocktype) {
        this.stocktype = stocktype;
    }

    @Override
    public String toString() {
        return getName();
    }
}
