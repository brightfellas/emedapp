package ke.co.brightfellas.emed;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.DbAdapter;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.Item;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.PatientItem;
import ke.co.brightfellas.emed.ke.co.brightfellas.emed.model.PaymentView;

/**
 * Created by iUwej on 8/26/14.
 */
public class FragmentSelectItems  extends ListFragment {

   private AutoCompleteTextView txtSearch;
   private EditText txtQuantity;
   private Button btnSave,btnAdd;
   private TextView lblAmount;
   private ArrayAdapter<Item> itemArrayAdapter;
    private SimpleCursorAdapter cursorAdapter;
   private EmedApplication app;
   private ArrayAdapter<PatientItem> itemsAdapter;
    private ArrayList<PatientItem> addedItems;

   private double total=0;
   private long visitId;

   private ProgressDialog dialog;
   private DbAdapter db;
    private Item selectedItem=null;




    interface OnProceduresSavedListener{

        public void proceduresAdded(long visitId);
    }

    OnProceduresSavedListener navListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        navListener= (OnProceduresSavedListener) activity;
    }

    Item selected=null;
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        db=new DbAdapter(getActivity());
        addedItems= new ArrayList<>();
        registerForContextMenu(getListView());
        if(savedInstanceState==null){
            visitId=getArguments().getLong("visit_id");

        }
        else{
            visitId=savedInstanceState.getLong("visit_id");

        }
        app=(EmedApplication)getActivity().getApplication();
        cursorAdapter= new SimpleCursorAdapter(getActivity(),R.layout.two_item_list_template,null,new String[]{"name"},new int[]{R.id.txt_header});
        itemArrayAdapter=new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,new ArrayList<Item>());
        txtSearch.setAdapter(cursorAdapter);

        cursorAdapter.setCursorToStringConverter( new SimpleCursorAdapter.CursorToStringConverter() {
            @Override
            public CharSequence convertToString(Cursor cursor) {
                final int columnIndex=cursor.getColumnIndexOrThrow("name");
                return cursor.getString(columnIndex);
            }
        });

        cursorAdapter.setFilterQueryProvider( new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence charSequence) {

                String constraints=charSequence==null?null: charSequence.toString();
                return db.findItem(constraints);
            }
        });



        txtSearch.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Cursor c= (Cursor)cursorAdapter.getItem(i);
                        selected=Item.createFromCursor(c);

            }
        });

        txtSearch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor c= (Cursor) adapterView.getItemAtPosition(i);
                selected=Item.createFromCursor(c);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //selected=null;
            }
        });
        itemsAdapter= new ArrayAdapter<PatientItem>(getActivity(),android.R.layout.simple_list_item_1,new ArrayList<PatientItem>()){

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View view=convertView;
                if(view==null){
                    LayoutInflater inflater=(LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    view=inflater.inflate(R.layout.items_template,null);

                }
                PatientItem item=getItem(position);
                ((TextView)view.findViewById(R.id.lbl_item)).setText(item.getItem().getName());
                ((TextView)view.findViewById(R.id.lbl_qty)).setText(String.valueOf(item.getQuantity()));
                ((TextView)view.findViewById(R.id.lbl_amount)).setText("Ksh "+(item.getQuantity()*item.getItem().getPrice()));
                return view;
            }
        };

        setListAdapter(itemsAdapter);
        //setEmptyText("No item");


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        saveToDb();
                        navListener.proceduresAdded(visitId);
                    }
                });





            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItem();
            }
        });
        //getLoaderManager().initLoader(0,null,this);
       getListView().setDivider(getResources().getDrawable(R.drawable.black_background));
       getListView().setDividerHeight(1);
        new LoadItems().execute();





    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater= getActivity().getMenuInflater();
        inflater.inflate(R.menu.edit_items,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info= (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.remove:
                PatientItem pItem= itemsAdapter.getItem(info.position);
                db.open();
                db.deletePatientItem(visitId,pItem.getItemId());
                itemsAdapter.remove(pItem);
                addedItems.remove(pItem);
                itemsAdapter.notifyDataSetChanged();
                return true;

            default:
                return  super.onContextItemSelected(item);
        }
    }

    private void addItem() {

        if(selected==null || txtQuantity.getEditableText().toString().isEmpty()){
            Toast.makeText(getActivity(),"Item or quantity is required!",Toast.LENGTH_SHORT).show();
            return;
        }

        int quantity= Integer.valueOf(txtQuantity.getEditableText().toString());
        total+=selected.getPrice()*quantity;
        PatientItem item= new PatientItem(selected,quantity,visitId);
        itemsAdapter.add( item);
        addedItems.add(item);
        itemsAdapter.notifyDataSetChanged();
        selected=null;
        lblAmount.setText("Ksh "+total);
        txtSearch.setText("");
    }


    private void saveToDb(){
        dialog= ProgressDialog.show(getActivity(),"Emed","Saving to db...");
        for(PatientItem item: addedItems){
            long result=db.addVisitItem(item);
            Log.d("Drugs and Procedures :","Item added with id: "+result);
        }

        dialog.dismiss();
    }





    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("visit_id",visitId);

    }

    @Override
    public void onResume() {
        super.onResume();
        db.open();
    }

    @Override
    public void onPause() {
        super.onPause();
        db.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_items,container,false);
        txtSearch=(AutoCompleteTextView)v.findViewById(R.id.txt_search_items);
        txtQuantity=(EditText)v.findViewById(R.id.txt_quantity);
        btnSave=(Button)v.findViewById(R.id.btn_save);
        btnAdd=(Button)v.findViewById(R.id.btn_add);
        lblAmount=(TextView)v.findViewById(R.id.lbl_total);

        return v;
    }


    class LoadItems extends AsyncTask<Void,Void,Cursor>{

        @Override
        protected Cursor doInBackground(Void... voids) {
            db.open();
            return db.getItems();



        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            dialog.dismiss();
            if(cursor !=null){
                Log.d("Items: ",cursor.getCount()+" items");
                cursorAdapter.changeCursor(cursor);
                cursorAdapter.notifyDataSetChanged();
            }
            new LoadVisitItems().execute(visitId);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog= ProgressDialog.show(getActivity(),"Emed","loading item list...");
        }
    }

     class LoadVisitItems extends  AsyncTask<Long,Void,List<PatientItem>>{

         @Override
         protected List<PatientItem> doInBackground(Long... longs) {
             long visitId=longs[0];
             db.open();
             Cursor items=db.fetchVisitItems(visitId);
             Log.d("Visits Items found: ",items.getCount()+"");
             if(items.getCount()>0){
                 ArrayList<PatientItem> list= new ArrayList<>();

                 while(items.moveToNext()){
                     PatientItem patientItemitem=PatientItem.createFromCursor(items);
                     Cursor itemCursor=db.findItem(patientItemitem.getItemId());
                     itemCursor.moveToNext();
                     patientItemitem.setItem(Item.createFromCursor(itemCursor));
                     list.add(patientItemitem);
                 }
                 return  list;
             }
             return null;
         }

         @Override
         protected void onPreExecute() {
             super.onPreExecute();
             dialog= ProgressDialog.show(getActivity(),"Emed","Loading previous items...");
         }

         @Override
         protected void onPostExecute(List<PatientItem> patientItems) {
             dialog.dismiss();
            if(patientItems !=null){
                //populate the items adapter here
                for(PatientItem item:patientItems){
                    itemsAdapter.add(item);
                }
                itemsAdapter.notifyDataSetChanged();
            }
         }
     }

}
