package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;

import android.content.ContentValues;

/**
 * Created by iUwej on 9/9/14.
 */
public class PatientDiagnosis extends  ModelItem {

    private long _id;

    public long getVisit_id() {
        return visit_id;
    }

    private long visit_id;


    public long getDiagnosis_id() {
        return diagnosis_id;
    }

    private long diagnosis_id;

    public PatientDiagnosis(long id, long visit_id,long diagnosis_id){
        this._id=id;
        this.visit_id=visit_id;
        this.diagnosis_id=diagnosis_id;


    }

    public PatientDiagnosis(){}


    public PatientDiagnosis(long visit_id,long diagnosis_id){
        this.visit_id=visit_id;
        this.diagnosis_id=diagnosis_id;
    }




    @Override
    String getCreateSQL() {
        return "create table patientsdiagnosis(" +
                "_id integer primary key autoincrement," +
                "visit_id integer not null," +
                "diagnosis_id integer not null," +
                "sync_status integer not null);";

    }

    @Override
    String getDropSQL() {
        return "drop table if exists patientdiagnosis";
    }

    @Override
    ContentValues toContentValues() {
        ContentValues values= new ContentValues();

        values.put("visit_id",visit_id);
        values.put("diagnosis_id",diagnosis_id);
        values.put("sync_status",sync_status);



        return values;
    }

    public static final String[] COLS={"_id","visit_id","diagnosis_id","sync_status"};

    public static final  String TABLE_NAME="patientsdiagnosis";
}
