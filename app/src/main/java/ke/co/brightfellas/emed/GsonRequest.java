package ke.co.brightfellas.emed;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * Created by iUwej on 8/21/14.
 */
public class GsonRequest<T> extends Request<T> {

    private Type clazz;
    private Response.Listener<T> listener;
    private Map<String,String> headers;


    public GsonRequest(String url,Type clazz,Map<String,String> headers,Response.Listener<T> listener,Response.ErrorListener errListener){
        super(Method.GET,url,errListener);
        this.clazz=clazz;
        this.listener=listener;
        this.headers=headers;
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        if(response.statusCode !=200){
            Log.d("Error Status",response.statusCode+"");
            return Response.error(new VolleyError(response));
        }


        try {

            String json= new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            JSONObject jobject= new JSONObject(json);
            json=jobject.getString("message");
            //Log.d("Message",json);
            T object=ModelFactory.createFromJson(json,clazz);
            return Response.success(object,HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException e) {
            return Response.error(new VolleyError(e));
        }

    }
}
