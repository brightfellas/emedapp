package ke.co.brightfellas.emed.ke.co.brightfellas.emed.model;


import android.content.ContentValues;
import android.database.Cursor;

import java.text.SimpleDateFormat;
import java.util.Date;

import ke.co.brightfellas.emed.SkipSerialization;

/**
 * Created by iUwej on 8/19/14.
 */
public class Visit extends ModelItem{

    private long _id;
    private long id;

    public long getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(long patient_id) {
        this.patient_id = patient_id;
    }

    private long patient_id;
    @SkipSerialization
    private String created_at;
    private int status;

    private Patient patient;

    public long get_id() {
        return _id;
    }

    public Patient getPatient() {
        return patient;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Visit(){}
    public Visit(long _id,long id,long patient_id,int sync_status,String created_at,int status){
        this._id=_id;
        this.id=id;
        this.patient_id=patient_id;
        this.created_at=created_at;
        this.status=status;
        this.sync_status=sync_status;
    }
    public Visit(long patient_id){
        this.patient_id=patient_id;
        created_at= new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        status=0;
    }

    @Override
    String getCreateSQL() {
        return "create table visits(_id integer primary key autoincrement," +
                "id integer," +
                "patient_id integer," +
                "sync_status integer default 0," +
                "created_at text," +
                "status integer default 0);";

    }

    public static  final String[] COLS={"_id","id","patient_id","sync_status","created_at","status"};

    @Override
    ContentValues toContentValues() {
        ContentValues cv=new ContentValues();
        cv.put("id",id);
        cv.put("patient_id",patient_id);
        cv.put("created_at",created_at);
        cv.put("status",status);
        return cv;
    }

    public static final String TABLE_NAME="visits";

    @Override
    String getDropSQL() {
        return "drop table if exists visits";
    }

    public static Visit createFromCursors(Cursor visitCursor,Cursor patientCursor){

        Visit visit= new Visit(visitCursor.getLong(visitCursor.getColumnIndexOrThrow("_id")),visitCursor.getLong(1),visitCursor.getLong(2),visitCursor.getInt(3),visitCursor.getString(4),visitCursor.getInt(5));
        if(patientCursor !=null)
            visit.setPatient(Patient.createFromCursor(patientCursor));

        return visit;
    }
}
